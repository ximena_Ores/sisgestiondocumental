<?php
    include("../config/data_Conexion.php");
    include("../config/conexionBD.php");
    session_start();
	$_SESSION['rol']="";

    $user = isset($_POST['user']) ? $_POST['user'] : '';
    $password = isset($_POST['password']) ? $_POST['password'] : '';

    $ok = true;
    $messages = array();

    if ( !isset($user) || empty($user) ) {
        $ok = false;
        $messages[] = 'usuario vacio';
    }

    if ( !isset($password) || empty($password) ) {
        $ok = false;
        $messages[] = 'contraseña incorrecta';
    }

    if ($ok) {
        $sentencia = $base_de_datos->query("select * from vtaC_dcCentroCosto_Director where numeroEmpleado=$user");
        $personas = $sentencia->fetchAll(PDO::FETCH_OBJ);

        if (empty($personas)) 
        {
            $sentencia = $base_de_datos->query("select * from vtaC_dcCentroCosto_Editor where numeroEmpleado=$user");
            $personas = $sentencia->fetchAll(PDO::FETCH_OBJ);
            if (empty($personas)) 
            {
                $ok = false;
                $messages[] = 'Usuario Incorrecto';
                $res = 'null';
                $rol = 'null';
            }else{
                $ok = true;
                $messages[] = 'excelente!';
                $res = $personas[0];
                $rol = $personas[0]->rol;
                $idPersona = $personas[0]->idPersona;

                $idCentroCostoEditor=$personas[0]->idCentroCosto;
                $sentenciaCC = $base_de_datos->query("select * from vtaC_dcCentroCosto_Director where idCentroCosto=$idCentroCostoEditor");
                $infoCC = $sentenciaCC->fetchAll(PDO::FETCH_OBJ);
                
                
            }
        }
        else{
            $ok = true;
            $messages[] = 'excelente!';
            $res = $personas[0];
            $rol = 'Director';
            $infoCC=[''];
            $idPersona = $personas[0]->idPersona;
        }
    }
    $_SESSION['rol']=$rol;
    $_SESSION['name']=$personas[0]->nombreCompletoPMN;
    
    $_SESSION['idCentroCosto']=$personas[0]->idCentroCosto;
    
    $_SESSION['idPersona']=$idPersona;

    
    /*else{
        $ok = false;
        $messages[] = 'incorrecto!';
    }*/

    echo json_encode(
        array(
            'ok' => $ok,
            'messages' => $messages,
            'res' => $res,
            'rol' => $rol,
            'infoCC'=>$infoCC[0]
        )
    );
    
?>