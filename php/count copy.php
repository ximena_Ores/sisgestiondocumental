<?php
    require_once("../config/data_Conexion.php");
    require_once("../config/conexionBD.php");
    session_start();
    $type = isset($_POST['type']) ? $_POST['type'] : '';
    $idPersona=$_SESSION['idPersona'];
    //$type="btn_expedientes";
    $idCentroCosto=$_SESSION['idCentroCosto'];
    switch ($type){
        case "btn_expedientes":
            $sentencia = $base_de_datos->query("select count(*)Total from vtaC_dcExpediente where (idCentroCosto=$idCentroCosto)");
            $total = $sentencia->fetchAll(PDO::FETCH_OBJ);
        break;
        case "btn_recibidos":
            $sentencia = $base_de_datos->query("select count(*)Total from vtaC_dcDocumento where idCentroCostoDestino=$idCentroCosto and (estado='EN')");
            $total = $sentencia->fetchAll(PDO::FETCH_OBJ);
        break;
        case "btn_enviados":
            $sentencia = $base_de_datos->query("select count(*)Total from vtaC_dcDocumento where (idCentroCostoOrigen=$idCentroCosto) and (estado='EN')");
            $total = $sentencia->fetchAll(PDO::FETCH_OBJ);
        break;
        case "btn_borradores":
            $sentencia = $base_de_datos->query("select count(*)Total from vtaC_dcDocumento_Expediente_Completo where (idCentroCostoOrigen=$idCentroCosto) and estadoDocumento='ED' and idPersonaElaboro=$idPersona and idEstadoExpediente!='CE'");
            $total = $sentencia->fetchAll(PDO::FETCH_OBJ);
        break;
        case "btn_aprobar":
            $sentencia = $base_de_datos->query("select idEstadoExpediente, folio as folio, tipoDocumento as tipo, asunto, centroCostoOrigen as origen, centroCostoDestino as destino, fechaElaboracion as fecha, estadoDocumento as estado, idDocumento from vtaC_dcDocumento_Expediente_Completo where (idCentroCostoOrigen=$idCentroCosto) and estadoDocumento='PF'");
            $total = $sentencia->fetchAll(PDO::FETCH_OBJ);
        break;
        case "btn_acusado":
            $sentencia = $base_de_datos->query("select count(*)Total from vtaC_dcDocumento where (idCentroCostoOrigen=$idCentroCosto) and estado='LE'");
            $total = $sentencia->fetchAll(PDO::FETCH_OBJ);
        break;
        default:
            $total=0;
    }
	echo json_encode(
        array(
            'total' => $total[0]->Total,
            'opcion' => $type
        )
    );
?>

