<?php
    require_once("../config/data_Conexion.php");
    require_once("../config/conexionBD.php");
    session_start();
    $id = isset($_POST['id']) ? $_POST['id'] : '';

    $sentencia = $base_de_datos->query("select vtaC_dcCentroCosto_Director.inicialesCC from vtaC_dcDocumento_Copia 
                                        INNER JOIN vtaC_dcCentroCosto_Director
                                        on vtaC_dcCentroCosto_Director.idCentroCosto=vtaC_dcDocumento_Copia.idCentroCosto and vtaC_dcDocumento_Copia.idDocumento=$id");
    $personas = $sentencia->fetchAll(PDO::FETCH_OBJ);
    $i=0;
    foreach ($personas as $rs) {
        $arreglo[$i]=$rs->inicialesCC;
        $i++;
    }
    if($arreglo==null){
        $_SESSION['inicialesCC']=null;
    }else{
        $_SESSION['inicialesCC']=$arreglo;
    }
    
    
    echo json_encode($arreglo);
?>

