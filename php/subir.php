<?php

include("../config/data_Conexion.php");
include("../config/conexionBD.php");
session_start();

$archivo = $_FILES["image"];
$descripcion=$_POST['username'];
$fileTmpPath = $_FILES['uploadedFile']['tmp_name'];
$fileName = $_FILES['image']['name'];
$fileSize = $_FILES['image']['size'];
$fileType = $_FILES['image']['type'];
$fileNameCmps = explode(".", $fileName);
$fileExtension = strtolower(end($fileNameCmps));

$realizo=strtolower($_SESSION['name']);
$nombre = str_replace(".$fileExtension", "", $fileName);

if (isset($_FILES['image']) && $_FILES['image']['error'] === UPLOAD_ERR_OK){

    $allowedfileExtensions = array('jpg', 'gif', 'png', 'zip', 'txt', 'xls', 'doc', 'pdf', 'docx');
    if (in_array($fileExtension, $allowedfileExtensions)){
      if (file_exists("../public/".$descripcion)) {
          echo "El fichero existe";
      } else {
          mkdir("../public/".$descripcion, 0777);
          echo "El fichero se creo";
      }

      $resultado = move_uploaded_file($archivo["tmp_name"],"../public/".$descripcion."/".$archivo['name']);
      if ($resultado) {
            $sentencia=$base_de_datos->prepare("exec dcDatosDocumento_AdjuntoAgregar $descripcion, '../public/$descripcion/$fileName','$nombre', '$fileName', '$realizo', null,null");
            $respuesta = $sentencia->execute();
            //echo $descripcion, '../public/190/datos', 'archivo','fisico', '', @error output,@mensaje output'
            echo "Subido con éxito " . $descripcion. "--", $archivo['name'];
      } else {
          echo "Error al subir archivo";
      }
    }else{
    echo 'Tipo de archivo no valido solo las siguientes: ' . implode(',', $allowedfileExtensions);
    }

}else {
    echo 'Error al subir el archivo: ' . $_FILES['image']['error'];
}
?>
