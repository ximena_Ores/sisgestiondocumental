<?php
    require_once("../config/data_Conexion.php");
    require_once("../config/conexionBD.php");
    session_start();
    $type = isset($_POST['type']) ? $_POST['type'] : '';
    $idPersona=$_SESSION['idPersona'];
    //$type="btn_expedientes";
    $idCentroCosto=$_SESSION['idCentroCosto'];
    switch ($type){
        case "btn_expedientes":
            $sentencia = $base_de_datos->query("select count(*)Total from vtaC_dcExpediente where (idCentroCosto=$idCentroCosto) and estado!='CE'");
            $total = $sentencia->fetchAll(PDO::FETCH_OBJ);
        break;
        case "btn_recibidos":
            $sentencia = $base_de_datos->query("select count(*)Total from vtaC_dcDocumento_Expediente_Completo where idCentroCostoDestino=$idCentroCosto and estadoDocumento='EN' and idEstadoExpediente!='CE'");
            $total = $sentencia->fetchAll(PDO::FETCH_OBJ);
        break;
        case "btn_enviados":
            $sentencia = $base_de_datos->query("select count(*)Total from vtaC_dcDocumento_Expediente_Completo where idCentroCostoOrigen=$idCentroCosto and estadoDocumento='EN' and idEstadoExpediente!='CE'");
            $total = $sentencia->fetchAll(PDO::FETCH_OBJ);
        break;
        case "btn_borradores":
            $sentencia = $base_de_datos->query("select count(*)Total from vtaC_dcDocumento_Expediente_Completo where idCentroCostoOrigen=$idCentroCosto and estadoDocumento='ED' and idPersonaElaboro=$idPersona and idEstadoExpediente!='CE'");
            $total = $sentencia->fetchAll(PDO::FETCH_OBJ);
        break;
        case "btn_aprobar":
            $sentencia = $base_de_datos->query("select count(*)Total from vtaC_dcDocumento_Expediente_Completo where idCentroCostoOrigen=$idCentroCosto and estadoDocumento='PF' and idEstadoExpediente!='CE'");
            $total = $sentencia->fetchAll(PDO::FETCH_OBJ);
        break;
        case "btn_acusado":
            $sentencia = $base_de_datos->query("select count(*)Total from vtaC_dcDocumento_Expediente_Completo where (idCentroCostoOrigen=1125) and estadoDocumento='LE'");
            $total = $sentencia->fetchAll(PDO::FETCH_OBJ);
        break;
        default:
            $total=0;
    }
	echo json_encode(
        array(
            'total' => $total[0]->Total,
            'opcion' => $type
        )
    );
?>

