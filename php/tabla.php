<?php
    require_once("../config/data_Conexion.php");
    require_once("../config/conexionBD.php");
    session_start();
    $type = isset($_POST['type']) ? $_POST['type'] : '';
    $idCentroCosto=$_SESSION['idCentroCosto'];
    $idPersona=$_SESSION['idPersona'];
    switch ($type){
        case "persona":
            //$sentencia = $base_de_datos->query("select folio as folio, tipoDocumento as tipo, asunto, centroCostoOrigen as origen, centroCostoDestino as destino, fechaElaboracion as fecha, estado from vtaC_dcDocumento where (idCentroCostoOrigen=$idCentroCosto)");
            $sentencia = $base_de_datos->query("select * from vtaC_dcPersona where idPersona not in (select idPersona from vtaC_dcCentroCosto_Editor)");
            $personas = $sentencia->fetchAll(PDO::FETCH_OBJ);
        break;
        case "btn_configuracion":
            //$sentencia = $base_de_datos->query("select folio as folio, tipoDocumento as tipo, asunto, centroCostoOrigen as origen, centroCostoDestino as destino, fechaElaboracion as fecha, estado from vtaC_dcDocumento where (idCentroCostoOrigen=$idCentroCosto)");
            $sentencia = $base_de_datos->query("select * from vtaC_dcCentroCosto_Editor where idCentroCosto=$idCentroCosto order by nombreCompletoPMN asc");
            $personas = $sentencia->fetchAll(PDO::FETCH_OBJ);
        break;
        case "btn_expedientes":
            //$sentencia = $base_de_datos->query("select folio as folio, tipoDocumento as tipo, asunto, centroCostoOrigen as origen, centroCostoDestino as destino, fechaElaboracion as fecha, estado from vtaC_dcDocumento where (idCentroCostoOrigen=$idCentroCosto)");
            $sentencia = $base_de_datos->query("select seccion, serieArchivistica, titulo, fechaApertura, fechaConclusion, idExpediente from vtaC_dcExpediente where (idCentroCosto=$idCentroCosto) order by idExpediente desc");
            $personas = $sentencia->fetchAll(PDO::FETCH_OBJ);
        break;
        case "btn_recibidos":
            //$sentencia = $base_de_datos->query("select folio as folio, tipoDocumento as tipo, asunto, centroCostoOrigen as origen, centroCostoDestino as destino, fechaElaboracion as fecha, estado, idDocumento from vtaC_dcDocumento where idCentroCostoDestino=$idCentroCosto and (estado='EN' or estado='LE')");
            $sentencia = $base_de_datos->query("
            select vtaC_dcDocumento.folio as folio, vtaC_dcDocumento.tipoDocumento as tipo, vtaC_dcDocumento.asunto, vtaC_dcDocumento.centroCostoOrigen as origen, vtaC_dcDocumento.centroCostoDestino as destino, vtaC_dcDocumento.fechaElaboracion as fecha, vtaC_dcDocumento.estado, vtaC_dcDocumento.idDocumento from vtaC_dcDocumento INNER 
            JOIN vtaC_dcDocumento_Copia  on (vtaC_dcDocumento_Copia.idDocumento = vtaC_dcDocumento.idDocumento) 
            where vtaC_dcDocumento_Copia.idCentroCosto = $idCentroCosto and (vtaC_dcDocumento.estado = 'EN' or vtaC_dcDocumento.estado = 'LE')
            union
			select vtaC_dcDocumento.folio as folio, vtaC_dcDocumento.tipoDocumento as tipo, vtaC_dcDocumento.asunto, vtaC_dcDocumento.centroCostoOrigen as origen, vtaC_dcDocumento.centroCostoDestino as destino, vtaC_dcDocumento.fechaElaboracion as fecha, vtaC_dcDocumento.estado, vtaC_dcDocumento.idDocumento from vtaC_dcDocumento where idCentroCostoDestino=$idCentroCosto and (estado='LE' or estado='EN')");
            $personas = $sentencia->fetchAll(PDO::FETCH_OBJ);
        break;
        case "btn_enviados":
            //$sentencia = $base_de_datos->query("select folio as folio, tipoDocumento as tipo, asunto, centroCostoOrigen as origen, centroCostoDestino as destino, fechaElaboracion as fecha, estado, idDocumento from vtaC_dcDocumento where (idCentroCostoOrigen=$idCentroCosto) and (estado='EN')");
            $sentencia = $base_de_datos->query("select idEstadoExpediente, folio as folio, tipoDocumento as tipo, asunto, centroCostoOrigen as origen, centroCostoDestino as destino, fechaElaboracion as fecha, estadoDocumento as estado, idDocumento from vtaC_dcDocumento_Expediente_Completo where (idCentroCostoOrigen=$idCentroCosto) and estadoDocumento='EN' and idEstadoExpediente!='CE'");
            $personas = $sentencia->fetchAll(PDO::FETCH_OBJ);
        break;
        case "btn_acusado":
            //$sentencia = $base_de_datos->query("select folio as folio, tipoDocumento as tipo, asunto, centroCostoOrigen as origen, centroCostoDestino as destino, fechaElaboracion as fecha, estado, idDocumento from vtaC_dcDocumento where (idCentroCostoOrigen=$idCentroCosto) and (estado='LE')");
            $sentencia = $base_de_datos->query("select idEstadoExpediente, folio as folio, tipoDocumento as tipo, asunto, centroCostoOrigen as origen, centroCostoDestino as destino, fechaElaboracion as fecha, estadoDocumento as estado, idDocumento from vtaC_dcDocumento_Expediente_Completo where (idCentroCostoOrigen=$idCentroCosto) and estadoDocumento='LE' and idEstadoExpediente!='CE'");
            
            $personas = $sentencia->fetchAll(PDO::FETCH_OBJ);
            
        break;
        case "btn_borradores":
            //$sentencia = $base_de_datos->query("select folio as folio, tipoDocumento as tipo, asunto, centroCostoOrigen as origen, centroCostoDestino as destino, fechaElaboracion as fecha, estado, idDocumento from vtaC_dcDocumento where (idCentroCostoOrigen=$idCentroCosto) and estado='ED' and idPersonaElaboro=$idPersona");
            $sentencia = $base_de_datos->query("select idEstadoExpediente, folio as folio, tipoDocumento as tipo, asunto, centroCostoOrigen as origen, centroCostoDestino as destino, fechaElaboracion as fecha, estadoDocumento as estado, idDocumento from vtaC_dcDocumento_Expediente_Completo where (idCentroCostoOrigen=$idCentroCosto) and estadoDocumento='ED' and idEstadoExpediente!='CE' and idPersonaElaboro=$idPersona");
            
            $personas = $sentencia->fetchAll(PDO::FETCH_OBJ);
        break;
        case "btn_aprobar":
            $sentencia = $base_de_datos->query("select idEstadoExpediente, folio as folio, tipoDocumento as tipo, asunto, centroCostoOrigen as origen, centroCostoDestino as destino, fechaElaboracion as fecha, estadoDocumento as estado, idDocumento from vtaC_dcDocumento_Expediente_Completo where idCentroCostoOrigen=$idCentroCosto and estadoDocumento='PF' and idEstadoExpediente!='CE'");
            $personas = $sentencia->fetchAll(PDO::FETCH_OBJ);
            
        break;
        default:
            echo "esta regla es por defecto";
    }
    if( !$personas){
        $personas[0] = ['folio' => '', 'tipo' => '', 'asunto' => '', 'origen' => '', 'destino' => '', 'fecha' => '',  'estado' => '',  'idDocumento' => '', 'idEstadoExpediente'=>''];
    }
    foreach ($personas as $rs) {
		$data =$rs;
		$arreglo['data'][]=$data;
    }

	echo json_encode($arreglo);
?>

