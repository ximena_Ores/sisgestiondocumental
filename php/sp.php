<?php
    require_once("../config/data_Conexion.php");
    require_once("../config/conexionBD.php");
    session_start();
    $titulo = isset($_POST['titulo']) ? $_POST['titulo'] : '';
    $resumen = isset($_POST['resumen']) ? $_POST['resumen'] : '';
    $observaciones = isset($_POST['observaciones']) ? $_POST['observaciones'] : '';
    $ubicacion = isset($_POST['ubicacion']) ? $_POST['ubicacion'] : '';
    $idSerieArchivistica = isset($_POST['idSerieArchivistica']) ? $_POST['idSerieArchivistica'] : '';
    $idCentroCosto=$_SESSION['idCentroCosto'];
    $tipoExpediente = isset($_POST['tipoExpediente']) ? $_POST['tipoExpediente'] : '';
    $usuarioRealizo =  $_SESSION['name'];
    $idExpediente = isset($_POST['idExpediente']) ? $_POST['idExpediente'] : '';
    $ip = $_SERVER['REMOTE_ADDR'];
    $host = $_SERVER['REMOTE_HOST'];
    
    if($idExpediente==''){
        $sentencia = $base_de_datos->prepare("exec dcDatosExpedienteAgregarModificar '$titulo', '$resumen', '$observaciones', '$ubicacion', $idSerieArchivistica, $idCentroCosto, $tipoExpediente, '$ip', '$host', '$usuarioRealizo', null, null, null");
        $respuestaSentencia = $sentencia->execute();
        $sentenciaID=$base_de_datos->query("select top(1) * from vtaC_dcExpediente order by idExpediente desc");
        $respuestaID = $sentenciaID->fetchAll(PDO::FETCH_OBJ);
    }
    else if($idExpediente!=''){
        $sentencia = $base_de_datos->prepare("exec dcDatosExpedienteAgregarModificar '$titulo', '$resumen', '$observaciones', '$ubicacion', $idSerieArchivistica, $idCentroCosto, $tipoExpediente, '$ip', '$host', '$usuarioRealizo', $idExpediente, null, null");
        $respuestaSentencia = $sentencia->execute();
        $sentenciaID=$base_de_datos->query("select * from vtaC_dcExpediente where idExpediente=$idExpediente");
        $respuestaID = $sentenciaID->fetchAll(PDO::FETCH_OBJ);
    }

    echo json_encode(
        array(
            'respuestaSentencia' => $respuestaSentencia,
            'expediente' => $respuestaID
         )
    );
?>

