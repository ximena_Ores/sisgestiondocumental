<?php

    session_start();
    $fecha = isset($_POST['fecha']) ? $_POST['fecha'] : '';
    $folio = isset($_POST['folio']) ? $_POST['folio'] : '';
    $asunto = isset($_POST['asunto']) ? $_POST['asunto'] : '';
    $destinoName = isset($_POST['destinoName']) ? $_POST['destinoName'] : '';
    $destinoPuesto = isset($_POST['destinoPuesto']) ? $_POST['destinoPuesto'] : '';
    $destinoPuestoIngles = isset($_POST['destinoPuestoIngles']) ? $_POST['destinoPuestoIngles'] : '';
    $mensaje = isset($_POST['mensaje']) ? $_POST['mensaje'] : '';
    $atencionA=isset($_POST['atencionA']) ? $_POST['atencionA'] : '';
    $origenName = isset($_POST['origenName']) ? $_POST['origenName'] : '';
    $origenPuesto = isset($_POST['origenPuesto']) ? $_POST['origenPuesto'] : '';
    $origenPuestoIngles = isset($_POST['origenPuestoIngles']) ? $_POST['origenPuestoIngles'] : '';
    $cc = isset($_POST['cc']) ? $_POST['cc'] : '';
    $tipoDoc = isset($_POST['tipoDoc']) ? $_POST['tipoDoc'] : '';
    $idCC = isset($_POST['idCC']) ? $_POST['idCC'] : '';
    $estado = isset($_POST['estado']) ? $_POST['estado'] : '';
    $firma = isset($_POST['firma']) ? $_POST['firma'] : '';
    $idPersona = isset($_POST['idPersona']) ? $_POST['idPersona'] : '';
    $inicialesCC = isset($_POST['inicialesCC']) ? $_POST['inicialesCC'] : '';
    
    $idExpediente = isset($_POST['idExpediente']) ? $_POST['idExpediente'] : '';

    
    $_SESSION['fecha_doc']=$fecha;
    $_SESSION['folio_doc']=$folio;
    $_SESSION['asunto_doc']=$asunto;
    $_SESSION['destinoName_doc']=$destinoName;
    $_SESSION['destinoPuesto_doc']=$destinoPuesto;
    $_SESSION['destinoPuestoIngles_doc']=$destinoPuestoIngles;
    $_SESSION['mensaje_doc']=$mensaje;
    $_SESSION['atencionA']=$atencionA;
    $_SESSION['origenName_doc']=$origenName;
    $_SESSION['origenPuesto_doc']=$origenPuesto;
    $_SESSION['origenPuestoIngles_doc']=$origenPuestoIngles;
    $_SESSION['tipoDoc']=$tipoDoc;
    $_SESSION['idCC']=$idCC;
    $_SESSION['estado']=$estado;
    $_SESSION['firma']=$firma;


    /*$ok = true;
    $messages = array();

    if ( !isset($fecha) || empty($fecha)) {
        $ok = false;
        $messages[] = 'fecha vacia';
    }else{
        $_SESSION['documento']=$fecha;
    }
    if ( !isset($folio) || empty($folio)) {
        $ok = false;
        $messages[] = 'folio vacia';
    }else{
        $_SESSION['folio']=$folio;
    }
    if ( !isset($asunto) || empty($asunto)) {
        $ok = false;
        $messages[] = 'asunto vacia';
    }else{
        $_SESSION['asunto']=$asunto;
    }
    echo json_encode(
        array(
            'ok' => $ok,
            'messages' => $messages,
            'fecha' => $fecha,
            'folio' => $folio,
            'asunto' => $asunto

         )
    );*/
    echo json_encode(
        array(
            'fecha' => $fecha,
            'folio' => $folio,
            'asunto' => $asunto,
            'destinoName' => $destinoName,
            'destinoPuesto' => $destinoPuesto,
            'destinoPuestoIngles' => $destinoPuestoIngles,
            'mensaje' => $mensaje,
            'atencionA' => $atencionA,
            'origenName' => $origenName,
            'origenPuesto' => $origenPuesto,
            'origenPuestoIngles' => $origenPuestoIngles,
            'cc' => $cc,
            'tipoDoc' => $tipoDoc,
            'idCC' => $idCC,
            'estado' => $estado,
            'firma' => $firma,
            'idPersona' => $idPersona,
            'inicialesCC' => $inicialesCC

         )
    );
    /*
    unset($_SESSION['fecha_doc']);
    unset($_SESSION['folio_doc']);
    unset($_SESSION['asunto_doc']);
    unset($_SESSION['destinoName_doc']);
    unset($_SESSION['destinoPuesto_doc']);
    unset($_SESSION['destinoPuestoIngles_doc']);
    unset($_SESSION['mensaje_doc']);
    unset($_SESSION['origenName_doc']);
    unset($_SESSION['origenPuesto_doc']);
    unset($_SESSION['origenPuestoIngles_doc']);*/
?>