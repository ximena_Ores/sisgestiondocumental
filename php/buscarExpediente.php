<?php
    include("../config/data_Conexion.php");
    include("../config/conexionBD.php");
    session_start();

    $id=$_SESSION['idCentroCosto'];
    $idExpediente =isset($_POST['type']) ? $_POST['type'] : '';
    $idPersona=$_SESSION['idPersona'];
    if($_SESSION['rol'] == 'Editor'){
      $sentencia=$base_de_datos->query("if exists (select * from vtaC_dcExpediente where idExpediente = $idExpediente and estado = 'ED' and idCentroCosto=$id)
                                        select 'carpeta vacia' as estadoCarpeta, idExpediente, titulo from vtaC_dcExpediente where idExpediente = $idExpediente
                                        else if exists (select * from vtaC_dcExpediente where idExpediente = $idExpediente and idCentroCosto!=$id)
                                            select 'carpeta sin existencia' as estadoCarpeta
                                            else if exists (select * from vtaC_dcExpediente where (idExpediente = $idExpediente and estado = 'AB' and idCentroCosto=$id) or (idExpediente = $idExpediente and estado = 'CE' and idCentroCosto=$id) )
                                                SELECT 'carpeta existe' as estadoCarpeta, vtaC_dcExpediente.idExpediente, vtaC_dcExpediente.estado as idEstadoExpediente, vtaC_dcDocumento_Expediente.titulo, vtaC_dcDocumento_Expediente.estadoExpediente, vtaC_dcDocumento_Expediente.idCentroCosto, vtaC_dcDocumento.folio as folio, vtaC_dcDocumento.tipoDocumento as tipo, vtaC_dcDocumento.asunto, vtaC_dcDocumento.centroCostoOrigen as origen, vtaC_dcDocumento.centroCostoDestino as destino, vtaC_dcDocumento.fechaElaboracion as fecha, vtaC_dcDocumento.estado, vtaC_dcDocumento.idDocumento 
                                                FROM vtaC_dcDocumento_Expediente 
                                                INNER JOIN vtaC_dcDocumento ON vtaC_dcDocumento_Expediente.idDocumento = vtaC_dcDocumento.idDocumento 
                                                INNER JOIN vtaC_dcExpediente ON vtaC_dcDocumento_Expediente.idExpediente = vtaC_dcExpediente.idExpediente 
                                                where vtaC_dcDocumento_Expediente.idExpediente = $idExpediente and vtaC_dcDocumento.idPersonaElaboro=$idPersona and vtaC_dcDocumento.estado!='CA' and vtaC_dcDocumento.estado!='PF' and vtaC_dcDocumento_Expediente.idCentroCosto=$id order by fecha desc");
                                          $personas = $sentencia->fetchAll(PDO::FETCH_OBJ);
    }else{
      $sentencia=$base_de_datos->query("if exists (select * from vtaC_dcExpediente where idExpediente = $idExpediente and estado = 'ED' and idCentroCosto=$id)
                                          select 'carpeta vacia' as estadoCarpeta, idExpediente, titulo from vtaC_dcExpediente where idExpediente = $idExpediente
                                          else if exists (select * from vtaC_dcExpediente where idExpediente = $idExpediente and idCentroCosto!=$id)
                                              select 'carpeta sin existencia' as estadoCarpeta
                                              else if exists (select * from vtaC_dcExpediente where (idExpediente = $idExpediente and estado = 'AB' and idCentroCosto=$id) or (idExpediente = $idExpediente and estado = 'CE' and idCentroCosto=$id) )
                                                  SELECT 'carpeta existe' as estadoCarpeta, vtaC_dcExpediente.idExpediente, vtaC_dcExpediente.estado as idEstadoExpediente, vtaC_dcDocumento_Expediente.titulo, vtaC_dcDocumento_Expediente.estadoExpediente, vtaC_dcDocumento_Expediente.idCentroCosto, vtaC_dcDocumento.folio as folio, vtaC_dcDocumento.tipoDocumento as tipo, vtaC_dcDocumento.asunto, vtaC_dcDocumento.centroCostoOrigen as origen, vtaC_dcDocumento.centroCostoDestino as destino, vtaC_dcDocumento.fechaElaboracion as fecha, vtaC_dcDocumento.estado, vtaC_dcDocumento.idDocumento 
                                                  FROM vtaC_dcDocumento_Expediente 
                                                  INNER JOIN vtaC_dcDocumento ON vtaC_dcDocumento_Expediente.idDocumento = vtaC_dcDocumento.idDocumento 
                                                  INNER JOIN vtaC_dcExpediente ON vtaC_dcDocumento_Expediente.idExpediente = vtaC_dcExpediente.idExpediente 
                                                  where vtaC_dcDocumento_Expediente.idExpediente = $idExpediente and vtaC_dcDocumento.estado!='CA' and vtaC_dcDocumento.estado!='ED' and vtaC_dcDocumento_Expediente.idCentroCosto=$id
                                                  UNION
                                                  SELECT 'carpeta existe' as estadoCarpeta, vtaC_dcExpediente.idExpediente, vtaC_dcExpediente.estado as idEstadoExpediente, vtaC_dcDocumento_Expediente.titulo, vtaC_dcDocumento_Expediente.estadoExpediente, vtaC_dcDocumento_Expediente.idCentroCosto, vtaC_dcDocumento.folio as folio, vtaC_dcDocumento.tipoDocumento as tipo, vtaC_dcDocumento.asunto, vtaC_dcDocumento.centroCostoOrigen as origen, vtaC_dcDocumento.centroCostoDestino as destino, vtaC_dcDocumento.fechaElaboracion as fecha, vtaC_dcDocumento.estado, vtaC_dcDocumento.idDocumento 
                                                  FROM vtaC_dcDocumento_Expediente 
                                                  INNER JOIN vtaC_dcDocumento ON vtaC_dcDocumento_Expediente.idDocumento = vtaC_dcDocumento.idDocumento 
                                                  INNER JOIN vtaC_dcExpediente ON vtaC_dcDocumento_Expediente.idExpediente = vtaC_dcExpediente.idExpediente 
                                                  where vtaC_dcDocumento_Expediente.idExpediente = $idExpediente and vtaC_dcDocumento.estado='ED' and vtaC_dcDocumento_Expediente.idCentroCosto=$id and vtaC_dcDocumento.idPersonaElaboro=$idPersona order by fecha desc
                                                  ");
                                        $personas = $sentencia->fetchAll(PDO::FETCH_OBJ);
    }
    foreach ($personas as $rs) {
		$data =$rs;
		$arreglo['data'][]=$data;
    }

	echo json_encode($arreglo);
?>

