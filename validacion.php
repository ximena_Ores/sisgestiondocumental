<?php
    $folio = isset($_GET['folio']) ? $_GET['folio'] : '';
    $estado = isset($_GET['e']) ? $_GET['e'] : '';
    $firma = isset($_GET['f']) ? $_GET['f'] : '';    


    include("config/data_Conexion.php");
    include("config/conexionBD.php");

    function desencrypt($password, $hash){
        if(password_verify($password, $hash)){
            return true;
        }else{
            return false;
        }
    }
    $estado_des=desencrypt($folio, $estado);
    $firma_des=desencrypt($folio, $firma);
    if($estado_des and $firma_des){
        $sentencia=$base_de_datos->query("select idCentroCostoOrigen, centroCostoOrigen, idCentroCostoDestino, centroCostoDestino, folio, estado, tipoDocumento from vtaC_dcDocumento where folio='$folio'");
        $documento = $sentencia->fetchAll(PDO::FETCH_OBJ);
        if($documento[0]->estado === 'LE' || $documento[0]->estado === 'EN'){
            $mensaje='<img src="src/images/svg/correcto.svg" width="70px" /><br><p>El documento con el folio '.$folio.'</p><p>Es correctamente firmado por '. $documento[0]->centroCostoOrigen.'</p>';
        }else{
            $mensaje='<img src="src/images/svg/incorrecto.svg" width="70px" /><br><p>El documento con el folio '.$folio.'</p><p>No existe</p>';
        }
    }else{
        $mensaje='<img src="src/images/svg/incorrecto.svg" width="70px" /><br><p>El documento con</p><p> el folio '.$folio.' no existe</p>';
    }
 ?>

<DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
    <link rel=StyleSheet href="src/css/index.css"/>
    <link  rel="icon"   href="src/user.png" type="image/png"/>
    <title> Información de Trayectorias Escolares</title>
</head>
<body>
    <section class="header">
        <p>Universidad Autónoma del Estado de Hidalgo</p>
    </section>
    <article class="body">
        <section class="modal-validar">
            <div>
                <?php echo $mensaje?>
            </div>
        </section>
    </article>
</body>
</html>