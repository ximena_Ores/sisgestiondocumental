//valores que se obtienen en el login para iniciar sesión
let form = {
    user: document.getElementById('user'),
    password: document.getElementById('password'),
    submit: document.getElementById('btn_submit'),
    messages: document.getElementById('form-messages')
};

form.submit.addEventListener('click', () => {

    const request = new XMLHttpRequest();
    request.onload = () => {
        let responseObject = null;
        try {
        responseObject = JSON.parse(request.responseText);
        } catch (e) {
        console.error('Could not parse JSON!');
        alert('Could not parse JSON!');
        }
        if (responseObject) {
            handleResponse(responseObject);
        }
    };
    const requestData = `user=${form.user.value}&password=${form.password.value}`;
    
    request.open('post', 'php/login.php');
    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    request.send(requestData);
});
const handleResponse = responseObject =>{
  console.log(responseObject);

    if (responseObject.ok) {
        console.log(responseObject.res);
        console.log(responseObject.rol);
        getBlock(responseObject, responseObject.rol);
    } else {
      while (form.messages.firstChild) {
        form.messages.removeChild(form.messages.firstChild);
      }
      responseObject.messages.forEach((message) => {
        const li = document.createElement('p');
        li.textContent = message;
        form.messages.appendChild(li);
        
      });
      
      form.messages.style.display = "block";
      let element = document.getElementById("form-messages");
      element.classList.add("alert__Error");
        setTimeout(() => { document.getElementById("form-messages").style.display = "none" }, 1600);
        
    }
}
const getBlock = (responseObject, rol) => {
  let sesion;
    
    if(rol=='Director'){
      sesion = {
        rol: rol,
        name: responseObject.res.nombreCompletoNPM,
        tituloAcademico: responseObject.res.tituloAcademicoAbreviado,
        puesto: responseObject.res.puesto,
        puestoIngles: responseObject.res.puestoIngles,
        correo: responseObject.res.correoElectronico,
        centroCosto: responseObject.res.centroCosto,
        idCentroCosto: responseObject.res.idCentroCosto,
        idPersona: responseObject.res.idPersona,
        inicialesCC: responseObject.res.inicialesCC,
        numeroEmpleado: responseObject.res.numeroEmpleado,
      };
    }if(rol=='Editor' || rol=='Supervisor'){
      sesion = {
        rol: rol,
        name: responseObject.res.nombreCompletoPMN,
        centroCosto: responseObject.res.centroCosto,
        nameDirector: responseObject.infoCC.nombreCompletoNPM,
        correo: responseObject.infoCC.correoElectronico,
        idCentroCosto: responseObject.infoCC.idCentroCosto,
        idPersona: responseObject.res.idPersona,
        idPersonaDirector: responseObject.infoCC.idPersona,
        inicialesCC: responseObject.infoCC.inicialesCC,
        puesto: responseObject.infoCC.puesto,
        puestoIngles: responseObject.infoCC.puestoIngles,
        tituloAcademico: responseObject.infoCC.tituloAcademicoAbreviado,
        numeroEmpleado: responseObject.infoCC.numeroEmpleado,
      };
    }
    console.log(responseObject)
    window.location.replace("index.html");

    let info = typeof window !== 'undefined' ? JSON.parse(localStorage.getItem('infoSesion')) : null;
    if (!info) {
        info = [];
    }
    if (info) {
        localStorage.setItem('infoSesion', JSON.stringify(sesion));
    } else {
        localStorage.setItem('infoSesion', JSON.stringify([]));
    }
    //console.log(rol);
    //console.log(responseObject.centroCosto);
    /*var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          //console.log(this.responseText)
            document.getElementById("windows").innerHTML = this.responseText;
        }
      };
    xhttp.open("GET", "menu.html", true);
    xhttp.send();*/
}