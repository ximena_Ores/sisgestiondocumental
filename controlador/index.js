const meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
let titulo;
let puesto;
let puestoIngles;
let tipo_expediente="";
let serieArchivistica="";
/***************************************************************************
    función de validar expediente
****************************************************************************/
const validarExpediente = (e) =>{
    let info = JSON.parse(localStorage.getItem('infoSesion'));
    requestInsert('php/validarCarpeta.php',`idDocumento=${e.id}&idCentroCosto=${info.idCentroCosto}`,modalExpediente, e, '');
    requestInsert('php/download.php',`idDocumento=${e.id}`,btnDownload, null, null);
}
/***************************************************************************
    función de vizualizar documento
****************************************************************************/

const visualizar = (e) =>{    
    const request = new XMLHttpRequest();
    request.onload = () => {
        let responseObject = null;
        try {
        responseObject = JSON.parse(request.responseText);
        } catch (e) {
        console.error('Could not parse JSON!');
        }
        if (responseObject) {
            let atencion=responseObject[0].atencionA;
            let id=responseObject[0].idDocumento;
            requestInsert('php/getCopia.php', `id=${id}`, imprimirD, null,null)
            atencion === undefined ? atencion='' : null
            
            let fecha=new Date(responseObject[0].fechaElaboracion);
            let returnData;
            requestDataServer('php/get_destino.php', `folio=${responseObject[0].idCentroCostoDestino}`, 'no');
            const destinoTitulo=titulo;
            const destinoPuesto=puesto;
            const destinoPuestoIngles=puestoIngles;
            requestDataServer('php/get_destino.php', `folio=${responseObject[0].idCentroCostoOrigen}`, 'no');
            const origenTitulo=titulo;
            const origenPuesto=puesto;
            const origenPuestoIngles=puestoIngles;
            
            returnData = `fecha=${fecha.getDate()} de ${meses[fecha.getMonth()]} de ${fecha.getFullYear()}&folio=${responseObject[0].folio}`;
            returnData += `&asunto=${responseObject[0].asunto}&destinoName=${destinoTitulo} ${responseObject[0].nombrePersonaDestino.toLowerCase().replace(/\b\w/g, l => l.toUpperCase())}`;
            returnData += `&destinoPuesto=${destinoPuesto}&destinoPuestoIngles=${destinoPuestoIngles}`;
            returnData += `&mensaje=${responseObject[0].contenido}&origenName=${origenTitulo} ${responseObject[0].nombrePersonaOrigen.toLowerCase().replace(/\b\w/g, l => l.toUpperCase())}`;
            returnData += `&origenPuesto=${origenPuesto}&origenPuestoIngles=${origenPuestoIngles}`;
            returnData += `&cc=enespera&tipoDoc=${responseObject[0].tipoDocumento}&idCC=${responseObject[0].idCentroCostoOrigen}&estado=${responseObject[0].estado}`;
            returnData += `&firma=${responseObject[0].idCentroCostoDestino}&atencionA=${atencion}`;
            visualizarDoc(null, returnData, id);
            
        }
    };
    const requestData = `type=btn_ver&idDocumento=${e.id}`;
    request.open('post', 'php/ver_documento.php', false);
    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    request.send(requestData);
}

/***************************************************************************************************
si está iniciada una sesión ver qué mostrará
***************************************************************************************************/
 
let info = typeof window !== 'undefined' ? JSON.parse(localStorage.getItem('infoSesion')) : null;
    if (!info) {
        info = [];
    }
    if (info) {
        switch (info.rol) {
            case 'Director':
                getVistas("menu", "vistas/vistas_rol/Director.php");
                director();
            break;
            case 'Editor':
                getVistas("menu", "vistas/vistas_rol/Editor.php");
                editor();
            break;
            case 'Supervisor':
                getVistas("menu", "vistas/vistas_rol/Supervisor.php");
                supervisor();
            break;
            default:
                alert("disculpa existio un error");
                window.location.replace("login.html");
        }
        document.getElementById("name").innerHTML= `<span id="${info.rol}">${info.name}</span>`;
    } 
    if(info.length == 0) {
        window.location.replace("login.html");
    }
/*****************************************************************************
	función con xmlhttPRecuest que obtiene vistas con parametro del id del
	contenedor y ruta/nombre de la vista
/*****************************************************************************/
function getVistas(contenedor, vista){
	const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(aaync) {
    	if (this.readyState == 4 && this.status == 200) {
            document.getElementById(contenedor).innerHTML = this.responseText;
		}
    };
	xhttp.open("GET", vista, false);
	xhttp.send();
}

const outSesion = document.querySelector('#btn__out');
    outSesion.addEventListener('click', function (){
        let info = typeof window !== 'undefined' ? JSON.parse(localStorage.getItem('infoSesion')) : null;
        if (info) {
            localStorage.setItem('infoSesion', JSON.stringify([]));
        }
        localStorage.clear();
        window.location.replace("login.html");
});
/*****************************************************************************
	querySelector obtiene el contenido de la etiqueta con el ID o CLASS,
    agregamos el evento con el addEventList la función que manda a llamar.
    crea el btn y manda la nueva vista a un contenedor.
/*****************************************************************************/
function btnCrear(nombre, funcion, content, view, event){
    const btn = document.querySelector(`#${nombre}`);
    
    btn.addEventListener(`${event}`, function (){
        let vista = typeof window !== 'undefined' ? localStorage.getItem('vista') : null;
        if(vista){
            console.log(vista)
            let elementEliminar = document.querySelector(`#${vista}`); 
            elementEliminar.classList.remove('btnSeleccionado');
        }
        localStorage.setItem('vista', `${nombre}`);
        console.log(nombre)
        funcion(content,view);
        let elementoActual = document.querySelector(`#${nombre}`); 
            elementoActual.classList.add('btnSeleccionado');
    });
    if(nombre!='btn_configuracion'){
        requestInsert("php/count.php", `type=${nombre}`, setCount, null, null);
    }
}
/*****************************************************************************
	Actualiza si existen nuevos mensajes a la hora que está lista la página
/*****************************************************************************/
let btnCount=['btn_recibidos', 'btn_aprobar', 'btn_enviados', 'btn_expedientes', 'btn_borradores'];
document.addEventListener("DOMContentLoaded", function() {
    //pos
	document.oncontextmenu = function(){return false}
    setInterval(function(){
        
        //let idExpedienteLS = typeof window !== 'undefined' ? localStorage.getItem('idExpedienteLS') : null;
        btnCount.map(item => requestInsert("php/count.php", `type=${item}`, setCount, null, null));
    },60000);
    
  });
/*****************************************************************************
    funciones para llevar a llamar los botones al hacer clic para los 
    diferentes tipos de usuarios
/*****************************************************************************/
function director(){ 
    btnCrear("btn_recibidos", getVistas, "cont_body", "vistas/vistas_view_documentos/recibidos.html","click");
    btnCrear("btn_aprobar", getVistas, "cont_body", "vistas/vistas_view_documentos/aprobar.html","click");
    btnCrear("btn_enviados", getVistas, "cont_body", "vistas/vistas_view_documentos/enviados.html","click");
    btnCrear("btn_acusado", getVistas, "cont_body", "vistas/vistas_view_documentos/acusados.html","click");
    btnCrear("btn_expedientes", getVistas, "cont_body", "vistas/vistas_view_documentos/expedientes.html","click");
    btnCrear("btn_borradores", getVistas, "cont_body", "vistas/vistas_view_documentos/borradores.html","click");
    btnCrear("btn_configuracion", getVistas, "cont_body", "vistas/vistas_view_documentos/configuracion.html","click");
}
function editor(){
    btnCrear("btn_expedientes", getVistas, "cont_body", "vistas/vistas_view_documentos/expedientes.html","click");
    btnCrear("btn_borradores", getVistas, "cont_body", "vistas/vistas_view_documentos/borradores.html","click");
}
function supervisor(){
    btnCrear("btn_recibidos", getVistas, "cont_body", "vistas/vistas_view_documentos/recibidos.html","click");
    btnCrear("btn_aprobar", getVistas, "cont_body", "vistas/vistas_view_documentos/aprobar.html","click");
    btnCrear("btn_enviados", getVistas, "cont_body", "vistas/vistas_view_documentos/enviados.html","click");
    btnCrear("btn_acusado", getVistas, "cont_body", "vistas/vistas_view_documentos/acusados.html","click");
    btnCrear("btn_expedientes", getVistas, "cont_body", "vistas/vistas_view_documentos/expedientes.html","click");
    btnCrear("btn_borradores", getVistas, "cont_body", "vistas/vistas_view_documentos/borradores.html","click");
}

/***************************************************************************
    función de btn para mandar a llamar la tabla correspondiente
****************************************************************************/
$("#btn_configuracion").on("click", function(){
    show__table_configuracion("#tableR","btn_configuracion");
});
$("#btn_expedientes").on("click", function(){
    show__table_Expediente("#tableR","btn_expedientes");
});
$("#btn_recibidos").on("click", function(){
    show__table("#tableR","btn_recibidos", "php/tabla.php");
});
$("#btn_aprobar").on("click", function(){
    show__table("#tableR","btn_aprobar", "php/tabla.php");
});
$("#btn_enviados").on("click", function(){
    show__table("#tableR","btn_enviados", "php/tabla.php");
});
$("#btn_borradores").on("click", function(){
    show__table("#tableR","btn_borradores", "php/tabla.php");
});
$("#btn_acusado").on("click", function(){
    show__table("#tableR","btn_acusado", "php/tabla.php");
});
/*****************************************************************************
	coloca los datos en el formulario de crear un nuevo documento o editarlo
/*****************************************************************************/
const pushSelect = (responseObject) =>{
    let respuesta=JSON.parse(responseObject);
    console.log(respuesta);
    localStorage.setItem('idCCDestino', respuesta[0].idCentroCosto);
    let name=respuesta[0].nombreCompletoNPM.toLowerCase().replace(/\b\w/g, l => l.toUpperCase());
    document.querySelector('#nameDir').defaultValue = `${respuesta[0].tituloAcademicoAbreviado} ${name}`;
    document.querySelector('#puestodes').defaultValue = `${respuesta[0].puesto}`;
    document.querySelector('#puestodesI').defaultValue = `${respuesta[0].puestoIngles}`;        
}
/***************************************************************************
    coloca los datos en el form de crear documento 
****************************************************************************/
const setDataDocumento = (responseObject) =>{
    //console.log(JSON.parse(responseObject));
    let {idTipoDocumento, idCentroCostoDestino, asunto, contenido, idDocumento} = JSON.parse(responseObject);
    localStorage.setItem('idCCDestino', idCentroCostoDestino);
    document.getElementById("selectCC").value = idCentroCostoDestino;
    document.getElementById("tipoDoc").value =idTipoDocumento;
    document.getElementById("asunto").value =asunto;
    document.getElementById("mensaje").value =contenido;
    requestInsert('php/get_destino.php',`folio=${idCentroCostoDestino}`,pushSelect, null, null);
            console.log(idCentroCostoDestino)
    //requestDataServer('php/get_destino.php', `folio=${idCentroCostoDestino}`, 'select');
 }
 const setDataDocumentoAtencionA = (responseObject) =>{ 
    let atencion=JSON.parse(responseObject);
    if(JSON.parse(responseObject) !== null){
        document.getElementById("atencionA").value =atencion.atencionA;
    }
 }
 /***************************************************************************
    Regresa los datos del expediente y los coloca en el modal
****************************************************************************/
const setDataExpediente = (responseObject) =>{
    let {idSeccion, seccion, idSerieArchivistica,  serieArchivistica, titulo, resumen, observaciones, idTipoExpediente, ubicacionTopografica} = JSON.parse(responseObject);
    document.getElementById("selectSeccion").value = idSeccion;
    document.getElementById("titulo").value =titulo;
    document.getElementById("resumen").value =resumen;
    document.getElementById("observaciones").value =observaciones;
    document.getElementById(`${idTipoExpediente}`).checked = true;
    document.getElementById("ubicacion").value =ubicacionTopografica;
 }
 /***************************************************************************
    verifica qué estado tiene el expediente, existe o no
****************************************************************************/
const estadoExpediente = (responseObject) =>{
   let estado=JSON.parse(responseObject).data[0];
   const elementFloat = document.querySelector('#contenido'); 
    document.getElementById("titulo-Expediente").innerHTML = estado.titulo;
    if(estado.estadoCarpeta === 'carpeta existe'){
        elementFloat.classList.remove('flex', 'col');
        getVistas("contenido", "vistas/vistas_view_documentos/fileOpen.html");
        show__table("#tableExpediente",estado.idExpediente, "php/buscarExpediente.php");       
    }
    if(estado.estadoCarpeta === 'carpeta sin existencia'){
        console.log(table[0].estadoCarpeta);
        alert ("Expediente sin existencia")
    }
    if(estado.estadoCarpeta === 'carpeta vacia'){
        elementFloat.classList.add('flex', 'col');
        document.getElementById("contenido").innerHTML ='<img src="src/images/svg/carpeta_vacia.svg" width=30%/><br><h2>Expediente vacio</h2>';
    }
}
 /***************************************************************************
    vaciar el div de los archivos
****************************************************************************/
const vaciarBox = (responseObject) =>{
    document.getElementById("preview").innerHTML = "";
}
 /***************************************************************************
    muestra  en consola
****************************************************************************/
const imprimirD = (responseObject) =>{
    console.log(responseObject);
}
 /***************************************************************************
    verifica si el documento tiene archivos adjuntos y poderlos descargar
****************************************************************************/
const btnDownload = (responseObject) =>{
    let respuesta=JSON.parse(responseObject);
    if(respuesta !== null){
        let {idDocumento, nombreFisico}=respuesta;
        document.getElementById("div-download").style.display="block";
        document.getElementById("div-download").innerHTML =`<p>Descargar adjuntos </p><a href="./php/downloadFile.php?idDocumento=${idDocumento}&nombreFisico=${nombreFisico}"><img class="aDownload"src="src/images/svg/download.svg" width="35px" /></a>`;
    }else if(respuesta ===null){
        document.getElementById("div-download").style.display="none";
        document.getElementById("div-download").innerHTML ="";
    }
}
 /***************************************************************************
    coloca los archivos adjuntos que existen en div
****************************************************************************/
const setDataDocumentoCopiaPara = (responseObject) =>{
    let arrayCopia=JSON.parse(responseObject);
    let listaCopiaCC='', divLista ='';
        arrayCopia.map(item=>{
            listaCopiaCC+=`${item.idCentroCosto}-`;
            document.getElementById("listaCopiaCC").textContent = listaCopiaCC;
            divLista+=`<p class="center"><button id="${item.idCentroCosto}" onclick="quitarCopia('${item.centroCosto}',${item.idCentroCosto});" id="btnQuitar" class="btnMini btnQuitar" title="Eliminar"></button>${item.centroCosto}</p>`;            
            document.getElementById("div-lista").innerHTML =divLista;
        });
}
 /***************************************************************************
    Si existe algun archivo archivo deshabilita y habilita botones para un
    subir o no archivos
****************************************************************************/
const setFile = (responseObject) =>{
    let fileResponse=JSON.parse(responseObject);
    if(fileResponse != null){
        document.getElementById("file").style.display="none";
        document.getElementById("btnCargar").style.display="none";
        document.getElementById("btnCargar").style.backgroundColor="#D5DBDB";
        let input = document.getElementById('btnCargar');
            input.disabled = true;
        let fileResponse=JSON.parse(responseObject);
        let file =`<p class="center"><button id="${fileResponse.idDocumento}" onclick="deleteFile('${fileResponse.nombreFisico}',${fileResponse.idDocumento});" id="btnQuitar" class="btnMini btnQuitar" title="Eliminar"></button>${fileResponse.nombreFisico}</p>`;            
        document.getElementById("preview").innerHTML =file;
    }else{
        document.getElementById("file").style.display="block";
        let input = document.getElementById('btnCargar');
        input.disabled = false;
    }
}
 /***************************************************************************
    Armar el select para los expedientes disponibles del modal elegir expediente
****************************************************************************/
const setSelectExpediente = (responseObject) =>{
    let respuesta=JSON.parse(responseObject);
    let selecExpe='<select id="idExpediente"> <option disabled selected value> -- Selecciona una opción -- </option>';
            respuesta.map(item =>{
                selecExpe+= `<option value="${item.idExpediente}">${item.titulo}</option>`;
            });
            selecExpe+='</select>';
            document.getElementById("select_Expediente").innerHTML =selecExpe;  
}
 /***************************************************************************
    Saber si el documento recibido ya está guardado en algun expediente
    si no mostrar modal para guardar en alguno y si no mostrarlo
****************************************************************************/
const modalExpediente = (responseObject, e, b) =>{
    let respuesta=JSON.parse(responseObject);
    if(respuesta[0].estado != 'no encontrado'){
        visualizar(e);
    }
    else{
        location.href = "#modalCopia"; 
        let sesion = JSON.parse(localStorage.getItem('infoSesion'));
        let {idCentroCosto}=sesion;
        requestInsert('php/tituloExpediente.php', `idCC=${idCentroCosto}`, setSelectExpediente, null, null);
        const addDoctoExp = document.querySelector('#btn_add_doc');
        addDoctoExp.addEventListener('click', function (){
            let x=document.getElementById('idExpediente').value;
            requestInsert('php/actualizarExpediente.php', `idDocumento=${e.id}&idExpe=${x}&idCentroCosto=${idCentroCosto}`, imprimirD, null, null);
            location.href = "#"; 
            setTimeout(function(){ visualizar(e); }, 1000);
            
        });
    }
}
 /***************************************************************************
    Al crear o actualizar un documento el boton de siguiente para no realizar
    una acción en subir archivos
****************************************************************************/
const siguiente = () =>{
    Swal.fire('Se realizó correctamente', '', 'success'); 
    let elementBody2 = document.querySelector('body'); 
    elementBody2.classList.remove('swal2-height-auto');
    cerrarFloat();
}
 /***************************************************************************
    Al seleccionar un archivo se cargará y creará un listener
    para el boton de cargar estará listo para que se pueda
    enviar al php un form data con el fetch y recibir el file en 
    el php
****************************************************************************/
const upload = (estadoBtn, a) =>{
    if(estadoBtn){
        document.getElementById("btnCargar").style.display="none";
    }else{
        document.getElementById("btnCargar").style.display="block";
    }
    location.href = "#modalUpload"; 
    const $form = document.querySelector('#form')
    const $image = document.querySelector('#image')
    const $file = document.querySelector('#file')
    function renderImage(formData) {
      const file = formData.get('image')
      const image = URL.createObjectURL(file)
    }
    const $username = document.querySelector('#username')
    function renderUsername(formData) {
      const username = formData.get('username')
    }
    $file.addEventListener('change', (event) => {
      const formData = new FormData($form)
      renderImage(formData)
    })
    $form.addEventListener('submit', (event) => {
      event.preventDefault();
      const formData = new FormData(event.currentTarget)
      renderUsername(formData)
          fetch("php/subir.php", {
            method: 'POST',
            body: formData,
          })
              .then(respuesta => respuesta.text())
              .then(decodificado => {
                  console.log(decodificado);
                  location.href = "#"; 
                  setTimeout(function(){ 
                      if(a===0){
                        Swal.fire('Documento creado', '', 'success'); 
                      }else if(a===1){
                        Swal.fire('Documento actualizado', '', 'success'); 
                      }
                    let elementBody2 = document.querySelector('body'); 
                        elementBody2.classList.remove('swal2-height-auto');
                        cerrarFloat();
                    }, 1500);
              });
    })
}
/***************************************************************************
    abre la ventana para editar un nuevo documento
****************************************************************************/
const openEditDocument = (e) =>{
    localStorage.setItem('idEditDocumento', e.id);
    const elementFloat = document.querySelector('#float'); 
    document.getElementById("float").style.display="block";
    elementFloat.classList.add('animate__animated', 'animate__slideInRight');
    elementFloat.addEventListener('animationend', () => {
        document.getElementById("float").style.display="block";
    });
    getVistas("contenido", "vistas/vistas_view_documentos/crear.html");
    formarDoc();
    requestInsert('php/data_document.php','',selectCrearDocumento, null, null);
    requestInsert('php/getDataExpediente.php', `type=dataDocumento&id=${e.id}`, setDataDocumento, null, null);
    requestInsert('php/getDataExpediente.php', `type=dataAtencionA&id=${e.id}`, setDataDocumentoAtencionA, null, null);
    requestInsert('php/getDataExpediente.php', `type=copiaPara&id=${e.id}`, setDataDocumentoCopiaPara, null, null);
    requestInsert('php/getDataExpediente.php', `type=archivos&id=${e.id}`, setFile, null, null);
    document.getElementById("btn_crear_document").style.display="none";
}
/***************************************************************************
    abre la ventana para escribir un nuevo documento
****************************************************************************/
const openNewDocument = (e) =>{
    const elementFloat = document.querySelector('#float'); 
    document.getElementById("float").style.display="block";
    elementFloat.classList.add('animate__animated', 'animate__slideInRight');
    elementFloat.addEventListener('animationend', () => {
        document.getElementById("float").style.display="block";
    });
    getVistas("contenido", "vistas/vistas_view_documentos/crear.html");
    formarDoc();
    localStorage.setItem('idExpedientes', e.id);
    requestInsert('php/data_document.php','',selectCrearDocumento, null, null);
    document.getElementById("btn_edit_document").style.display="none";
    document.getElementById("listaCopiaCC").textContent = "";
    document.getElementById("div-lista").innerHTML ="";
}
/***************************************************************************
    abre la ventana de los expedientes 
****************************************************************************/
const openExp = (e) =>{
    localStorage.setItem('idExpedientes', e.id);
    requestInsert('php/buscarExpediente.php', `type=${e.id}`, estadoExpediente, null,null)
    const elementFloat = document.querySelector('#float'); 
    document.getElementById("float").style.display="block";
    elementFloat.classList.add('animate__animated', 'animate__slideInRight');
    elementFloat.addEventListener('animationend', () => {
        document.getElementById("float").style.display="block";
    });
}
/***************************************************************************
    Cierra las ventanas  flotantes 
****************************************************************************/
const cerrarFloat = () =>{
    let elementFloat2 = document.querySelector('#float'); 
    elementFloat2.classList.remove('animate__animated', 'animate__slideInRight');
    elementFloat2.classList.add('animate__animated', 'animate__slideOutRight');
    elementFloat2.addEventListener('animationend', () => {
        elementFloat2.classList.remove('animate__animated', 'animate__slideOutRight');
        document.getElementById("float").style.display="none";
    }); 
}
 /***************************************************************************
    Muestra la tabla de que son posibles elegir a un rol de editores
****************************************************************************/
const show__table_Editor = (id,e) =>{
    $(`${id}`).DataTable({
          "destroy":true,
          "ajax":{
            "method":"POST",
            "url":"php/tabla.php",
            "data" : {type: e}
          },
        "columns":[
            {"data":"numeroEmpleado"},
            {"data":"nombreCompletoPMN"},
            {"render":
                function ( data, type, row ) {
                    return ("<button id='" + row['numeroEmpleado'] + "'  onClick='agregarEditorTabla(this)' class='btnEstado deshabilitar'>Agregar</button>");    
                }
            },
        ], "language": idioma_espanol
        });
}
 /***************************************************************************
    Muestra la tabla de que actualmente tienen una cuenta como editor
    o supervisor
****************************************************************************/
const show__table_configuracion = (id,e) =>{
    $(`${id}`).DataTable({
          "destroy":true,
          "ajax":{
            "method":"POST",
            "url":"php/tabla.php",
            "data" : {type: e}
          },
        "columns":[
            {"data":"numeroEmpleado"},
            {"data":"nombreCompletoPMN"},
            {"data":"rol"},
            {"render":
                function ( data, type, row ) {

                    if(row['idRol']==3){
                        return("<div class='flex end'><button id='" + row['numeroEmpleado'] + "'  onClick='deshabilitar(this)' class='btnEstado deshabilitar'>Deshabilitar</button><button id='" + row['numeroEmpleado'] +"' title='Cancelar documento' class='btnEliminar btnMini' onClick=eliminarRol(this)></button></div>");
                    }else if(row['idRol']==1){
                        return("<div class='flex end '><button id='" + row['numeroEmpleado'] + "'  onClick='habilitar(this)' class='btnEstado habilitar'>Habilitar</button><button id='" + row['numeroEmpleado'] +"' title='Cancelar documento' class='btnEliminar btnMini' onClick=eliminarRol(this)></button></div>");
                    }
                    
                }
            },
        ], "language": idioma_espanol
        });
}
/***************************************************************************
    función para mostrar la tabla respectivamente de su opción que va al php
    y regresa la información
****************************************************************************/
const show__table_Expediente = (id,e) =>{
    $(`${id}`).DataTable({
          "destroy":true,
          "ajax":{
            "method":"POST",
            "url":"php/tabla.php",
            "data" : {type: e}
          },
        "columns":[
            {"data":"seccion"},
            {"data":"serieArchivistica"},
            {"data":"titulo"},
            {"render":
                function ( data, type, row ) {

                    if(row['fechaApertura']!=null){
                        let fecha=new Date(row['fechaApertura']);
                        return (`${fecha.getDate()}/${meses[fecha.getMonth()]}/${fecha.getFullYear()}`);
                    }else{
                        return (`---`);
                    }
                }
            },
            {"render":
                function ( data, type, row ) {
                    if(row['fechaConclusion'] == null & row['fechaApertura']==null){
                        return "---";
                    }
                    if(row['fechaConclusion'] == null){
                        return "Abierto";
                    }else{
                        let fecha=new Date(row['fechaConclusion']);
                        return (`${fecha.getDate()}/${meses[fecha.getMonth()]}/${fecha.getFullYear()}`);
                    }
                }
            },
            {"render":
                function ( data, type, row ) {
                    if(row['fechaConclusion'] == null & row['fechaApertura']==null){
                        return ("<div class='flex' ><button id='" + row['idExpediente'] +"' class='btnEditar btnMini' title='Vizualizar documento' onClick=editarExpediente(this)></button><button id='" + row['idExpediente'] +"' class='btnEliminar btnMini' title='Eliminar expediente' onClick=eliminarExpediente(this)></button><button id='" + row['idExpediente'] + "'  onClick='openNewDocument(this)' class='btnEstado nuevo'>Nuevo <img src='src/images/svg/agregarDocumento.svg' width=20px/></button><button id='" + row['idExpediente'] + "'  onClick='openExp(this)' class='btnEstado abrir'>Vizualizar<img src='src/images/svg/abrirExpediente.svg' width=20px/></button></div>");
                    }
                    if(row['fechaConclusion'] == null){
                        return ("<div class='flex' ><button id='" + row['idExpediente'] +"' class='btnEditar btnMini' title='Vizualizar documento' onClick=editarExpediente(this)></button><button id='" + row['idExpediente'] +"' class='btnCerrar btnMini' title='Cerrar expediente' onClick=cerrarExpediente(this)></button><!--button id='" + row['idExpediente'] +"' class='btnCerrar btnMini' title='Vizualizar documento' onClick=cerrarExpediente(this)></button--><button id='" + row['idExpediente'] + "'  onClick='openNewDocument(this)' class='btnEstado nuevo'>Nuevo <img src='src/images/svg/agregarDocumento.svg' width=20px/></button><button id='" + row['idExpediente'] + "'  onClick='openExp(this)' class='btnEstado abrir'>Vizualizar<img src='src/images/svg/abrirExpediente.svg' width=20px/></button></div>");
                    }else{
                        return ("<div class='flex end' ><button id='" + row['idExpediente'] + "'  onClick='openExp(this)' class='btnEstado abrir'>Vizualizar<img src='src/images/svg/abrirExpediente.svg' width=20px/></button></div>");
                    }
                }
            }
        ], "language": idioma_espanol
        });
}
 /***************************************************************************
    Muestra la tabla de los documentos
****************************************************************************/
const show__table = (id, e, url) =>{
    $(`${id}`).DataTable({
          "destroy":true,
          "ajax":{
            "method":"POST",
            "url":`${url}`,
            "data" : {type: e}
          },
        "columns":[
            {"data":"folio"},
            {"data":"tipo"},
            {"data":"asunto"},
            {"data":"origen"},
            {"data":"destino"},
            {"render":
                function ( data, type, row ) {
                    if(row['fecha'] == ''){
                        return (``);
                    }else{
                        let fecha=new Date(row['fecha']);
                        return (`${fecha.getDate()}/${meses[fecha.getMonth()]}/${fecha.getFullYear()}`);
                    }
                }
            },
            {"render":
                function ( data, type, row ) {
                    
                    if(row['estado'] == "ED"){
                        return ("<button id='ED' class='btnEstado ED'>Pendiente</button>");
                    }
                    if(row['estado'] == "EN"){
                        if(e=='btn_recibidos'){
                            return ("<button id='EN' class='btnEstado EN'>Recibido</button>");
                        }else{
                            return ("<button id='EN' class='btnEstado EN'>Enviado</button>");
                        }
                    }
                    if(row['estado'] == "PF"){
                        return ("<button id='PF' class='btnEstado ED' >Por aprobar</button>");
                    }
                    if(row['estado'] == "LE"){
                        return ("<button id='LE' class='btnEstado LE' >Leído</button>");
                    }
                    if(row['estado'] == ''){
                        return ("");
                    }
                }
            }, 
            {"render":
                function ( data, type, row ) {
                if(row['idEstadoExpediente'] != 'CE'){
                    if(row['estado'] == "ED"){
                        return ("<div class='flex'><button id='" + row['idDocumento'] +"' class='btnVer btnMini' title='Vizualizar documento' onClick=visualizar(this)></button><button id='" + row['idDocumento'] +"' class='btnEditar btnMini' title='Editar documento' onClick=openEditDocument(this)></button><button id='" + row['idDocumento'] +"' title='Enviar a firma' class='btnEnviar btnMini' onClick=changeStatusSendSignature(this)></button><button id='" + row['idDocumento'] +"' title='Cancelar documento' class='btnEliminar btnMini' onClick=cancelarDocumento(this)></button></div>");
                    }if(row['estado'] == "PF"){
                        return ("<div class='flex'><button id='" + row['idDocumento'] +"' class='btnVer btnMini' title='Vizualizar documento' onClick=visualizar(this)></button><button id='" + row['idDocumento'] +"' class='btnFirma btnMini' title='Firmar documento' onClick=changeStatusSignature(this)></button><button id='" + row['idDocumento'] +"' class='btnRegresarDoc btnMini' onClick=returnDoc(this) title='Regresar documento para edición'></button></div>");
                    }
                    if(row['estado'] == "LE"){
                        return ("<button id='" + row['idDocumento'] +"' class='btnVer btnMini' title='Vizualizar documento' onClick=validarExpediente(this)></button>");
                    }
                    if(row['estado'] == "EN" && e=='btn_recibidos'){
                        return ("<div class='flex'><!--button id='" + row['idDocumento'] +"' class='btnVer btnMini' title='Vizualizar documento' onClick=changeStateRead(this)></button--><button id='" + row['idDocumento'] + "'  onClick='acuseRecibido(this)' class='btnEstado acuse'>Acuse de recibido <img src='src/images/svg/acuse.svg' width=25px/></button></div>");
                    }
                    if(row['estado'] == "EN" && e=='btn_enviados'){
                        return ("<button id='" + row['idDocumento'] +"' class='btnVer btnMini' title='Vizualizar documento' onClick=visualizar(this)></button><!--button id='" + row['idDocumento'] + "'  onClick='acuseRecibido(this)' class='btnEstado acuse'>Acuse de recibido <img src='src/images/svg/acuse.svg' width=25px/></button>");
                    }else if(row['estado'] == "EN" && e!='btn_enviados' && e!='btn_recibidos'){
                        return ("<button id='" + row['idDocumento'] +"' class='btnVer btnMini' title='Vizualizar documento' onClick=visualizar(this)></button>");
                    }
                }
                if(row['idEstadoExpediente']==''){
                    return('');
                }
                else{
                    return ("<button id='" + row['idDocumento'] +"' class='btnVer btnMini' title='Vizualizar documento' onClick=visualizar(this)></button><p>Expediente cerrado</p>");
                }
                }
            }
        ], "language": idioma_espanol
        });
}
/***************************************************************************
    variable para mostrar en español la tabla
****************************************************************************/
const idioma_espanol = {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "No se encontró ningún registro",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}
 /***************************************************************************
    Abre modal y verifica si existe un archivo adjunto
****************************************************************************/
const mostrarModalIframe = (responseObject, id, b) =>{
    location.href = "#miModal"; 
    iframe.src = iframe.src; 
    requestInsert('php/download.php',`idDocumento=${id}`,btnDownload, null, null);
}
 /***************************************************************************
    Vizualiza el documento y lo muestra en el IFRAMe
****************************************************************************/
function  visualizarDoc(funcion, data, id){
    let sendData;
    if(data === null){
        sendData=funcion();
    }else{
        sendData=data;
    }
    /*Elimina los datos anteriores para poder cargar los datos del doc*/
    requestInsert('php/delete_data_doc.php', '', null, null, null)
    /*Carga nuevos datos*/
    requestInsert('php/send_data_doc.php',sendData, mostrarModalIframe, id, '')
}
 /***************************************************************************
    Elimina un CC ya cargado
****************************************************************************/
const quitarCopia = (a, b) =>{
    let listaCopiaCC = document.getElementById("listaCopiaCC").innerHTML;
    let divLista = document.getElementById("div-lista").innerHTML;
    let button=`<p class="center"><button id="${b}" onclick="quitarCopia('${a}',${b});" class="btnMini btnQuitar" title="Eliminar"></button>${a}</p>`;
    document.getElementById("listaCopiaCC").textContent = listaCopiaCC.replace(`${b}-`, "");
    document.getElementById("div-lista").innerHTML =divLista.replace(`${button}`, "");;
}
 /***************************************************************************
    verifica si existe documento adjunto para habilitar o desabilitar 
    el boton de subir archivo
****************************************************************************/
const deleteFile = (a, b) =>{
    requestInsert('php/eliminarFile.php', `id=${b}&nombre=${a}`, vaciarBox, null, null);
    document.getElementById("file").style.display="block";
    document.getElementById("btnCargar").style.display="block";
    document.getElementById("btnCargar").style.backgroundColor="#38b765";
    let input = document.getElementById('btnCargar');
        input.disabled = false;
}
 /***************************************************************************
    Forma el modal de copia y agrega un evento listener al select formado
    para poder ir agregando las copias de los CC
****************************************************************************/
const cargarModalCC = (object) =>{
    let responseObject = JSON.parse(object);
    let selectCentroCostos='<select id="selectCopia"> <option disabled selected value> -- Selecciona una opción -- </option>';
        responseObject.map(item =>{
            selectCentroCostos+= `<option value="${item.idCentroCosto}">${item.centroCosto}</option>`;
        });
        selectCentroCostos+='</select>';
        
        document.getElementById("select_copia").innerHTML =selectCentroCostos;
    let divLista='';
    let select = document.getElementById('selectCopia');
        select.addEventListener('change', function(){
            let selectedOption = this.options[select.selectedIndex];
            let listaCopiaCC='';
                listaCopiaCC = document.getElementById("listaCopiaCC").innerHTML;
                listaCopiaCC+=`${selectedOption.value}-`;
                divLista ='';
                divLista = document.getElementById("div-lista").innerHTML;
                document.getElementById("listaCopiaCC").textContent = listaCopiaCC;
                divLista+=`<p class="center"><button id="${selectedOption.value}" onclick="quitarCopia('${selectedOption.text}',${selectedOption.value});" id="btnQuitar" class="btnMini btnQuitar" title="Eliminar"></button>${selectedOption.text}</p>`;            
                document.getElementById("div-lista").innerHTML =divLista;
        });
}
 /***************************************************************************
    Abre el modal de agregar copia y prepara todo el form
    además verifica si existe ya algun CC ya registrado
****************************************************************************/
function copia(){
    location.href = "#modalCopia"; 
    requestInsert('php/data_document.php', '', cargarModalCC, null, null);
    
    /*const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(aaync) {
        if (this.readyState == 4 && this.status == 200) {
            let responseObject = JSON.parse(this.responseText);
            let selectCentroCostos='<select id="selectCopia"> <option disabled selected value> -- Selecciona una opción -- </option>';
            responseObject.map(item =>{
                selectCentroCostos+= `<option value="${item.idCentroCosto}">${item.centroCosto}</option>`;
            });
            selectCentroCostos+='</select>';
            document.getElementById("select_copia").innerHTML =selectCentroCostos;
            let divLista='';
            let select = document.getElementById('selectCopia');
            select.addEventListener('change', function(){
            let selectedOption = this.options[select.selectedIndex];
            let listaCopiaCC='';
            listaCopiaCC = document.getElementById("listaCopiaCC").innerHTML;
            console.log(listaCopiaCC);
            listaCopiaCC+=`${selectedOption.value}-`;
            divLista ='';
            divLista = document.getElementById("div-lista").innerHTML;
            document.getElementById("listaCopiaCC").textContent = listaCopiaCC;
            divLista+=`<p class="center"><button id="${selectedOption.value}" onclick="quitarCopia('${selectedOption.text}',${selectedOption.value});" id="btnQuitar" class="btnMini btnQuitar" title="Eliminar"></button>${selectedOption.text}</p>`;            
            document.getElementById("div-lista").innerHTML =divLista;
            });
        }
    };
    xhttp.open("GET", 'php/data_document.php', false);
    xhttp.send();*/
}
/***************************************************************************
    Recibe como parametros el nombre de la opción para consultar en el PHP
    el ID de la tabla a actualizar
****************************************************************************/
const updateTable = (responseObject, nameOpcion, idTabla) =>{    
    show__table(`${idTabla}`,`${nameOpcion}`,"php/tabla.php");
}
/***************************************************************************
    Función de expedientes para mostrar realizado correctamente
****************************************************************************/
const setElementHTML = (responseObject) =>{
    show__table_Expediente("#tableR","btn_expedientes");
    requestInsert("php/count.php", `type=btn_expedientes`, setCount, null, null);
}
/***************************************************************************
    Obtiene los valores del form de expediente para enviarlo a la función 
    requestInsert con los parametros de la ruta del PHP, los valores ya 
    obtenidos para el POST, la función a ejecutar, valores nullos que no
    requieren
****************************************************************************/
const sendExpediente = () =>{
    let documentFormExpediente = {
        titulo: document.getElementById('titulo').value,
        resumen: document.getElementById('resumen').value,
        observaciones: document.getElementById('observaciones').value,
        ubicacion: document.getElementById('ubicacion').value,
        idSerieArchivistica: document.getElementById('serieArchivistica').value,
        tipoExpediente: tipo_expediente.charAt(0),
        submit: document.getElementById('btn_expediente')
    };
    
    let requestData = `titulo=${documentFormExpediente.titulo}&resumen=${documentFormExpediente.resumen}`;
        requestData += `&observaciones=${documentFormExpediente.observaciones}&ubicacion=${documentFormExpediente.ubicacion}`;
        requestData += `&idSerieArchivistica=${documentFormExpediente.idSerieArchivistica}&tipoExpediente=${documentFormExpediente.tipoExpediente}`;
        requestData += `&idExpediente=`;
    documentFormExpediente.titulo==='' || documentFormExpediente.titulo.length > 250 ? requestData='' : null
    documentFormExpediente.resumen==='' || documentFormExpediente.resumen.length > 250 ? requestData='' : null
    documentFormExpediente.observaciones==='' || documentFormExpediente.observaciones.length > 250 ? requestData='' : null
    documentFormExpediente.idSerieArchivistica==='' ? requestData='' : null
    documentFormExpediente.tipoExpediente==='' ? requestData='' : null
    documentFormExpediente.tipoExpediente !=='E' && (documentFormExpediente.ubicacion ==='' || documentFormExpediente.ubicacion.length > 250) ? requestData='' : null
    documentFormExpediente.tipoExpediente ==='E' && (documentFormExpediente.ubicacion !=='' || documentFormExpediente.ubicacion.length > 250) ? requestData='' : null
    return requestData;   
}
const crearExpediente = () =>{
    let requestData=sendExpediente();
    if(!requestData){
        Swal.fire(
            '',
            'Llena correctamente los datos requeridos',
            'error'
          )
          let elementBody = document.querySelector('body'); 
          elementBody.classList.remove('swal2-height-auto');
    }else{
        Swal.fire({
            title: '¿Deseas crear el expediente?',
            showDenyButton: true,
            confirmButtonText: `Confirmar`
        }).then((result) => {
            if (result.isConfirmed) {
                requestInsert('php/sp.php', requestData, setElementHTML, null,null); 
                Swal.fire('expediente creado', '', 'success')
            }
            let elementBody2 = document.querySelector('body'); 
            elementBody2.classList.remove('swal2-height-auto');
        })
        let elementBody = document.querySelector('body'); 
        elementBody.classList.remove('swal2-height-auto');
    }
}
const editExpediente = () =>{
    let IDeditarExpediente = JSON.parse(localStorage.getItem('IDeditarExpediente'));
    let requestData=sendExpediente();
    if(!requestData){
        Swal.fire(
            '',
            'Llena correctamente los datos requeridos',
            'error'
          )
          let elementBody = document.querySelector('body'); 
          elementBody.classList.remove('swal2-height-auto');
    }else{
        requestData += `${IDeditarExpediente}`;
        Swal.fire({
            title: '¿Deseas editar el expediente?',
            showDenyButton: true,
            confirmButtonText: `Confirmar`
        }).then((result) => {
            if (result.isConfirmed) {
                requestInsert('php/sp.php', requestData, setElementHTML, null,null); 
                Swal.fire('expediente editado', '', 'success')
            }
            let elementBody2 = document.querySelector('body'); 
            elementBody2.classList.remove('swal2-height-auto');
        })
        let elementBody = document.querySelector('body'); 
        elementBody.classList.remove('swal2-height-auto');
    }
}
/***************************************************************************
    Función para regresar el documento a pendientes y evalua la tabla a
    actualizar
****************************************************************************/
const returnDoc = (event) => {
    let nameOpcionSpan=document.getElementById('nameOpcion').innerText;
    let idExpedienteLS = typeof window !== 'undefined' ? localStorage.getItem('idExpedienteLS') : null;
    if(nameOpcionSpan==='EXPEDIENTES'){
        Swal.fire({
            title: '¿Desea regresar el documento a edición?',
            showDenyButton: false,
            showCancelButton: true,
            confirmButtonText: `Confirmar`
        }).then((result) => {
            if (result.isConfirmed) {
                requestInsert('php/returnDoc.php', `id=${event.id}`, null, null, null);
                show__table("#tableExpediente",idExpedienteLS, "php/buscarExpediente.php");
                requestInsert("php/count.php", `type=btn_aprobar`, setCount, null, null);
                requestInsert("php/count.php", `type=btn_borradores`, setCount, null, null);
                Swal.fire('Documento regresado a edición correctamente', '', 'success')
            }
            let elementBody2 = document.querySelector('body'); 
            elementBody2.classList.remove('swal2-height-auto');
        })
        let elementBody = document.querySelector('body'); 
        elementBody.classList.remove('swal2-height-auto');        
    }
    if(nameOpcionSpan!='EXPEDIENTES'){

        Swal.fire({
            title: '¿Desea regresar el documento a edición?',
            showDenyButton: false,
            showCancelButton: true,
            confirmButtonText: `Confirmar`
        }).then((result) => {
            if (result.isConfirmed) {
                requestInsert('php/returnDoc.php', `id=${event.id}`, updateTable, "btn_aprobar", "#tableR");
                requestInsert("php/count.php", `type=btn_aprobar`, setCount, null, null);
                requestInsert("php/count.php", `type=btn_borradores`, setCount, null, null);
                Swal.fire('Documento regresado a edición correctamente', '', 'success')
            }
            let elementBody2 = document.querySelector('body'); 
            elementBody2.classList.remove('swal2-height-auto');
        })
        let elementBody = document.querySelector('body'); 
        elementBody.classList.remove('swal2-height-auto');
    }
}
/***************************************************************************
    Función para poner en estado a leer en documento y evalua la tabla a
    actualizar
****************************************************************************/
const acuseRecibido = (event) =>{
    Swal.fire({
        title: '¿Acusar de recibido?',
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: `Confirmar`
    }).then((result) => {
        if (result.isConfirmed) {
        Swal.fire('Documento enviado', '', 'success')
        changeStateRead(event);
        }
        let elementBody2 = document.querySelector('body'); 
        elementBody2.classList.remove('swal2-height-auto');
    })
    let elementBody = document.querySelector('body'); 
    elementBody.classList.remove('swal2-height-auto');
}

/***************************************************************************
    Función para poner en estado a editarExpediente
****************************************************************************/
const editarExpediente = (event) =>{
    nuevo_Expediente();
    requestInsert('php/getDataExpediente.php', `type=dataExpediente&id=${event.id}`, setDataExpediente, null, null);
    localStorage.setItem('IDeditarExpediente', event.id);
    document.getElementById("btn_expedienteCrear").style.display="none";
    document.getElementById("btn_expedienteEditar").style.display="block";
}
/***************************************************************************
    Función para poner en estado a cancelarDocumento
****************************************************************************/
const eliminarRol = (event) =>{
    Swal.fire({
        title: '¿Desea eliminar al editor?',
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: `Confirmar`
    }).then((result) => {
    if (result.isConfirmed) {
        console.log(event.id);
        requestInsert('php/eliminarRol.php', `id=${event.id}`, null, null, null);
        show__table_configuracion("#tableR","btn_configuracion");
            Swal.fire('Realizado correctamente', '', 'success')
        }
        let elementBody2 = document.querySelector('body'); 
        elementBody2.classList.remove('swal2-height-auto');
    })
    let elementBody = document.querySelector('body'); 
    elementBody.classList.remove('swal2-height-auto');
}
/***************************************************************************
    agregar a editor
****************************************************************************/
const agregarEditorTabla = (event) =>{
    Swal.fire({
        title: '¿Desea agregar a editor?',
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: `Confirmar`
    }).then((result) => {
    if (result.isConfirmed) {
        console.log(event.id);
        requestInsert('php/agregarEditor.php', `id=${event.id}`, null, null, null);
        show__table_configuracion("#tableR","btn_configuracion");
        show__table_Editor("#tableEditor","persona");
            Swal.fire('Se agrego correctamente como editor', '', 'success')
        }
        let elementBody2 = document.querySelector('body'); 
        elementBody2.classList.remove('swal2-height-auto');
    })
    let elementBody = document.querySelector('body'); 
    elementBody.classList.remove('swal2-height-auto');
    
}
/***************************************************************************
    Función para darle permisos de supervisor
****************************************************************************/
const habilitar = (event) =>{    
    Swal.fire({
        title: '¿Desea darle permisos de supervisor?',
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: `Confirmar`
    }).then((result) => {
    if (result.isConfirmed) {
        /*let sesion = JSON.parse(localStorage.getItem('infoSesion'));
        let {idCentroCosto}=sesion;
        console.log(event.id);
        console.log(idCentroCosto);*/
        requestInsert('php/habilitar.php', `idEmpleado=${event.id}`, imprimirD, null, null);
        show__table_configuracion("#tableR","btn_configuracion");
        Swal.fire('Se realizó correctamente', '', 'success')
        }
        let elementBody2 = document.querySelector('body'); 
        elementBody2.classList.remove('swal2-height-auto');
    })
    let elementBody = document.querySelector('body'); 
    elementBody.classList.remove('swal2-height-auto');   
}
/***************************************************************************
    Función para poner deshabilitar editor
****************************************************************************/
const deshabilitar = (event) =>{
    Swal.fire({
        title: '¿Desea darle permisos de editor?',
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: `Confirmar`
    }).then((result) => {
    if (result.isConfirmed) {
        requestInsert('php/eliminarRol.php', `id=${event.id}`, null, null, null);
        requestInsert('php/agregarEditor.php', `id=${event.id}`, null, null, null);
        show__table_configuracion("#tableR","btn_configuracion");
        Swal.fire('Se realizó correctamente', '', 'success')
    }
        let elementBody2 = document.querySelector('body'); 
        elementBody2.classList.remove('swal2-height-auto');
    })
    let elementBody = document.querySelector('body'); 
    elementBody.classList.remove('swal2-height-auto');
    
}
/***************************************************************************
    Función para poner en estado a cancelarDocumento
****************************************************************************/
const cancelarDocumento = (event) =>{
    console.log(event.id);
    Swal.fire({
        title: '¿Desea eliminar el documento?',
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: `Confirmar`
    }).then((result) => {
    if (result.isConfirmed) {
        requestInsert('php/eliminarDoc.php', `id=${event.id}`, null,null, null);
        requestInsert("php/count.php", `type=btn_borradores`, setCount, null, null);
        let idExpediente= localStorage.getItem('idExpedientes');
        let nameOpcionSpan=document.getElementById('nameOpcion').innerText;
        if(nameOpcionSpan!='PENDIENTES EN ENVIAR'){
            show__table("#tableExpediente",idExpediente, "php/buscarExpediente.php");
        }else{
            show__table("#tableR","btn_borradores", "php/tabla.php");
        }
        Swal.fire('Documento eliminado', '', 'success')
    }
        let elementBody2 = document.querySelector('body'); 
        elementBody2.classList.remove('swal2-height-auto');
    })
    let elementBody = document.querySelector('body'); 
    elementBody.classList.remove('swal2-height-auto');
    
}
/***************************************************************************
    Función para poner en estado a cancelarExpediente
****************************************************************************/
const cerrarExpediente = (event) =>{
 Swal.fire({
        title: '¿Desea cerrar el expediente?',
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: `Confirmar`
    }).then((result) => {
    if (result.isConfirmed) {
            requestInsert('php/cerrarExp.php', `id=${event.id}`, null, null, null);
            setElementHTML(null);
            Swal.fire('Expediente cerrado', '', 'success')
        }
        let elementBody2 = document.querySelector('body'); 
        elementBody2.classList.remove('swal2-height-auto');
    })
    let elementBody = document.querySelector('body'); 
    elementBody.classList.remove('swal2-height-auto');
    
}
/***************************************************************************
    Función para poner en estado a eliminarExpediente
****************************************************************************/
const eliminarExpediente = (event) =>{
    console.log(event.id);
 Swal.fire({
        title: '¿Desea eliminar expediente?',
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: `Confirmar`
    }).then((result) => {
    if (result.isConfirmed) {
            requestInsert('php/eliminarExp.php', `id=${event.id}`, null, null, null);
            setElementHTML(null);
            Swal.fire('Expediente eliminado', '', 'success')
        }
        let elementBody2 = document.querySelector('body'); 
        elementBody2.classList.remove('swal2-height-auto');
    })
    let elementBody = document.querySelector('body'); 
    elementBody.classList.remove('swal2-height-auto');
}
/***************************************************************************
    Función para poner en estado a leer en documento y evalua la tabla a
    actualizar
****************************************************************************/
const changeStateRead = (event) =>{
    let nameOpcionSpan=document.getElementById('nameOpcion').innerText;
    let idExpedienteLS = typeof window !== 'undefined' ? localStorage.getItem('idExpedienteLS') : null;
    if(nameOpcionSpan==='EXPEDIENTES'){
        requestInsert('php/leer.php', `id=${event.id}`, null, null, null);
        show__table("#tableExpediente",idExpedienteLS, "php/buscarExpediente.php");
    }
    if(nameOpcionSpan!='EXPEDIENTES'){
        requestInsert('php/leer.php', `id=${event.id}`, updateTable,"btn_recibidos", "#tableR");
    }
}
/***************************************************************************
    Función para poner en estado a firmado en documento y evalua la tabla a
    actualizar
****************************************************************************/
const changeStatusSignature = (event) =>{
    let nameOpcionSpan=document.getElementById('nameOpcion').innerText;
    let idExpedienteLS = typeof window !== 'undefined' ? localStorage.getItem('idExpedienteLS') : null;
    if(nameOpcionSpan==='EXPEDIENTES'){
        Swal.fire({
            title: '¿Desea firmar el documento?',
            showDenyButton: false,
            showCancelButton: true,
            confirmButtonText: `Confirmar`
        }).then((result) => {
            if (result.isConfirmed) {
                requestInsert('php/firma.php', `id=${event.id}`, null, null, null);
                show__table("#tableExpediente",idExpedienteLS, "php/buscarExpediente.php");
                requestInsert("php/count.php", `type=btn_aprobar`, setCount, null, null);
                requestInsert("php/count.php", `type=btn_enviados`, setCount, null, null);
                Swal.fire('Documento enviado', '', 'success')
            }
            let elementBody2 = document.querySelector('body'); 
            elementBody2.classList.remove('swal2-height-auto');
        })
        let elementBody = document.querySelector('body'); 
        elementBody.classList.remove('swal2-height-auto');
    }if(nameOpcionSpan!='EXPEDIENTES'){
        Swal.fire({
            title: '¿Desea regresar el documento a edición?',
            showDenyButton: false,
            showCancelButton: true,
            confirmButtonText: `Confirmar`
        }).then((result) => {
            if (result.isConfirmed) {
                requestInsert('php/firma.php', `id=${event.id}`, updateTable, "btn_aprobar", "#tableR");
                requestInsert("php/count.php", `type=btn_aprobar`, setCount, null, null);
                requestInsert("php/count.php", `type=btn_enviados`, setCount, null, null);
                Swal.fire('Documento enviado', '', 'success')
            }
            let elementBody2 = document.querySelector('body'); 
            elementBody2.classList.remove('swal2-height-auto');
        })
        let elementBody = document.querySelector('body'); 
        elementBody.classList.remove('swal2-height-auto');
    }
}
/***************************************************************************
    Función para poner en estado a por aprobar en documento y evalua la tabla a
    actualizar
****************************************************************************/
const changeStatusSendSignature = (event) =>{
        Swal.fire({
            title: '¿Desea enviar a firma el documento?',
            showDenyButton: false,
            showCancelButton: true,
            confirmButtonText: `Confirmar`
        }).then((result) => {
            if (result.isConfirmed) {
                requestInsert('php/mandarFirma.php', `id=${event.id}`, null, null, null);
                let idExpediente= localStorage.getItem('idExpedientes');
                let nameOpcionSpan=document.getElementById('nameOpcion').innerText;
                let sesion = JSON.parse(localStorage.getItem('infoSesion'));
                if(nameOpcionSpan!='PENDIENTES EN ENVIAR'){
                    show__table("#tableExpediente",idExpediente, "php/buscarExpediente.php");
                }else{
                    show__table("#tableR","btn_borradores", "php/tabla.php");
                }
                requestInsert("php/count.php", `type=btn_borradores`, setCount, null, null);
                if(sesion.rol!='Editor'){
                    requestInsert("php/count.php", `type=btn_aprobar`, setCount, null, null);
                }
                Swal.fire('Documento enviado', '', 'success')
            }
            let elementBody2 = document.querySelector('body'); 
            elementBody2.classList.remove('swal2-height-auto');
        })
        let elementBody = document.querySelector('body'); 
        elementBody.classList.remove('swal2-height-auto'); 
}

/***************************************************************************
    Pendiente igual imprumirD
****************************************************************************/
function imprimir(responseObject){
    console.log(responseObject)
    show__table_Expediente("#tableR","btn_expedientes");
}
window.onload = function(){
    if(!Push.Permission.GRANTED){
        Push.Permission.request();
    }
}
function notificacion(mensaje){
    Push.create("Gestión Documental", {
        body: mensaje,
        icon: 'src/images/svg/notificacion.png',
        timeout: 9000,
        vibrate: [100,100,100],
        onClick : function(){
            alert('Click en la notificiacion');
        }
    });
}
/***************************************************************************
    Verificador si existe un nuevo oficio en alguna bandeja 
    y muestra la notificacion
****************************************************************************/
function setCount(responseObject){//pos
    let {total, opcion}=JSON.parse(responseObject);
    total==='0' ? total= 0 : null
    let bandeja = opcion.replace(`btn_`, "");
        bandeja==='aprobar' ? bandeja= 'pendiente por aprobar' : null
    let mensaje= total > 1 ? `${total} Nuevos oficios en bandeja de ${bandeja}` :  `${total} Nuevo oficio en bandeja de ${bandeja}`
    let resta=0;
    if(opcion ==='btn_recibidos' ||  opcion ==='btn_aprobar' || opcion === 'btn_acusado'){
        let info = typeof window !== 'undefined' ? JSON.parse(localStorage.getItem(`${opcion}`)) : null
        info === null && total!==0 
            ? (notificacion(mensaje), localStorage.setItem(`${opcion}`, total), document.getElementById(`count_${opcion}`).innerHTML =total) 
            : (console.log("local:", info, " nuevo:", total), total > info 
                ? ( resta=Number(total) -  Number(info),
                    mensaje=`${resta} Nuevo mensaje en bandeja de ${bandeja}`, 
                    notificacion(mensaje),
                    document.getElementById(`count_${opcion}`).innerHTML =total,
                    localStorage.setItem(`${opcion}`, total)
                  ) 
                : (console.log("no paso nada"),document.getElementById(`count_${opcion}`).innerHTML =total))
    }else{document.getElementById(`count_${opcion}`).innerHTML =total}
}
/***************************************************************************
    Función para crear documento. 1° se obtiene el objeto del documento 
    de la función formMessage. 2° con la función requestInsert envía a 
    la ruta del php, el objeto del documento, la función a ejecutar, valores
    nulos 
****************************************************************************/
function crearDocumento(){
    let documentos=formMessage();
    if(!documentos){
        Swal.fire(
            '',
            'Llena correctamente los datos requeridos',
            'error'
          )
          let elementBody = document.querySelector('body'); 
          elementBody.classList.remove('swal2-height-auto');
    }else{
        Swal.fire({
            title: '¿Deseas crear el documento?',
            showDenyButton: true,
            confirmButtonText: `Confirmar`
        }).then((result) => {
            if (result.isConfirmed) {
                let array=getCopia();
                requestInsert('php/crearDocumento.php',documentos,im, array, '');
                requestInsert("php/count.php", `type=btn_borradores`, setCount, null, null);
                show__table_Expediente("#tableR","btn_expedientes");
                upload(false, 0);                
            }
            let elementBody2 = document.querySelector('body'); 
            elementBody2.classList.remove('swal2-height-auto');
        })
        let elementBody = document.querySelector('body'); 
            elementBody.classList.remove('swal2-height-auto');
    }
} 
/***************************************************************************
    Función para obtener los destino y los copia
****************************************************************************/
function im(objeto, array, b){
    let respuesta = JSON.parse(objeto);
    if(respuesta.id){
        document.querySelector('#username').defaultValue =respuesta.id;
        if(array!=''){
           array.map(item=>{
                const request = new XMLHttpRequest();
                request.onload = () => {
                    let responseObject = null;
                    try {
                    responseObject = request.responseText;
                    } catch (e) {
                        console.error('Could not parse JSON!'); 
                    }
                    if (responseObject) {
                        let persona = JSON.parse(responseObject);
                        let datoscopia= `idDocumento=${Number(respuesta.id)}&idCentroCosto=${Number(item)}`;
                        datoscopia+= `&idPersona=${persona[0].idPersona}`;
                        console.log(datoscopia);
                        requestInsert('php/addCopia.php',datoscopia,null, null, null)
                    }
                };
                request.open('post', 'php/get_destino.php', false);
                request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                request.send(`folio=${Number(item)}`);
            });
        }
    }
}
/***************************************************************************
    Función para actualizar documento. 1° se obtiene el objeto del documento 
    de la función formMessage. 2° con la función requestInsert envía a 
    la ruta del php, el objeto del documento, la función a ejecutar, valores
    nulos 
****************************************************************************/
function actualizarDocumento(){
    let documento=formMessageEditar();
    if(!documento){
        Swal.fire(
            '',
            'Llena correctamente los datos requeridos',
            'error'
          )
          let elementBody = document.querySelector('body'); 
          elementBody.classList.remove('swal2-height-auto');
    }else{
        Swal.fire({
            title: '¿Deseas actualizar el documento?',
            showDenyButton: true,
            confirmButtonText: `Confirmar`
        }).then((result) => {
            if (result.isConfirmed) {
                let array=getCopia();
                requestInsert('php/editDocumento.php',documento,im, array, '');
                let idExpediente= localStorage.getItem('idExpedientes');
                let nameOpcionSpan=document.getElementById('nameOpcion').innerText;
                if(nameOpcionSpan!='PENDIENTES EN ENVIAR'){
                    show__table("#tableExpediente",idExpediente, "php/buscarExpediente.php");
                }else{
                    show__table("#tableR","btn_borradores", "php/tabla.php");
                }
                upload(false, 1);
            }
            let elementBody2 = document.querySelector('body'); 
            elementBody2.classList.remove('swal2-height-auto');
        })
        let elementBody = document.querySelector('body'); 
            elementBody.classList.remove('swal2-height-auto');
    }
} 
/***************************************************************************
    ajax para mandar a cualquier php con datos post y función a ejecutar
    con parametros a y b
****************************************************************************/
function requestInsert(ruta, requestData, funcion, a, b){
    const request = new XMLHttpRequest();
    request.onload = () => {
        let responseObject = null;
        try {
        responseObject = request.responseText;
        } catch (e) {
            console.error('Could not parse JSON!'); 
        }
        if (responseObject) {
            if(funcion===null && a === null && b===null){
                console.log("vacio null");
            }else if(funcion !==null && a === null && b===null){
                funcion(responseObject);
            }if(funcion !==null && a!= null && b!=null){
                funcion(responseObject,a,b); 
            }
        }
    };
    request.open('post', ruta, false);
    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    request.send(requestData);
}
/***************************************************************************
    Obtiene el FormData y lo enviar ya formado
****************************************************************************/
function formMessageEditar(){
    
    let sesion = JSON.parse(localStorage.getItem('infoSesion'));
    let idCCDestino = localStorage.getItem('idCCDestino');
    let idDocumento=localStorage.getItem('idEditDocumento');
    let {rol, idPersona, inicialesCC, idPersonaDirector,idCentroCosto} = sesion;
    console.log(idCCDestino);
    let documentForm;
    if(rol=='Director'){
        documentForm = {
            asunto: document.getElementById('asunto'),
            mensaje: document.getElementById('mensaje'),
            atencionA: document.getElementById('atencionA'),
            tipoDoc: document.getElementById('tipoDoc'),
            idPersona: `${idPersona}` ,
            inicialesCC: `${inicialesCC}`,
            idCCDestino: idCCDestino,
            submit: document.getElementById('btn_submit_document')
        };
    }else{
        documentForm = {
            asunto: document.getElementById('asunto'),
            mensaje: document.getElementById('mensaje'),
            atencionA: document.getElementById('atencionA'),
            tipoDoc: document.getElementById('tipoDoc'),
            idPersona: `${idPersona}` ,
            inicialesCC: `${inicialesCC}`,
            idCCDestino: idCCDestino,
            submit: document.getElementById('btn_submit_document')
        };
    }
    
        let requestData =`asunto=${documentForm.asunto.value}`;
        requestData += `&mensaje=${documentForm.mensaje.value}&atencionA=${documentForm.atencionA.value}`;
        requestData += `&tipoDoc=${documentForm.tipoDoc.value}`;
        requestData += `&firma=${idCentroCosto}`;
        requestData += `&idPersona=${documentForm.idPersona}&inicialesCC=${documentForm.inicialesCC}`;
        requestData += `&idCCDestino=${documentForm.idCCDestino}&idDocumento=${idDocumento}`;

        documentForm.asunto.value ==='' || documentForm.asunto.value.length > 250 ? requestData='' : null
        documentForm.mensaje.value ==='' ? requestData='' : null
        documentForm.tipoDoc.value==='' ? requestData='' : null
        documentForm.idPersona ==='' ? requestData='' : null
        documentForm.inicialesCC ==='' ? requestData='' : null
        documentForm.idCCDestino==='' ? requestData='' : null
    return requestData;
}
/***************************************************************************
    Ontener el objeto del nuevo focumento
****************************************************************************/
function formMessage(){
    let sesion = JSON.parse(localStorage.getItem('infoSesion'));
    let idCCDestino = localStorage.getItem('idCCDestino');
    let idExpediente= localStorage.getItem('idExpedientes');
    let {rol, idPersona, inicialesCC, idPersonaDirector,idCentroCosto} = sesion;

    let documentForm;
    if(rol=='Director'){
        documentForm = {
            asunto: document.getElementById('asunto'),
            mensaje: document.getElementById('mensaje'),
            atencionA: document.getElementById('atencionA'),
            tipoDoc: document.getElementById('tipoDoc'),
            idPersona: `${idPersona}` ,
            inicialesCC: `${inicialesCC}`,
            idExpediente: idExpediente,
            idCCDestino: idCCDestino,
            submit: document.getElementById('btn_submit_document')
        };
    }else{
        documentForm = {
            asunto: document.getElementById('asunto'),
            mensaje: document.getElementById('mensaje'),
            atencionA: document.getElementById('atencionA'),
            tipoDoc: document.getElementById('tipoDoc'),
            idPersona: `${idPersona}` ,
            inicialesCC: `${inicialesCC}`,
            idExpediente: idExpediente,
            idCCDestino: idCCDestino,
            submit: document.getElementById('btn_submit_document')
        };
    }
        let requestData = `asunto=${documentForm.asunto.value}`;
        requestData += `&mensaje=${documentForm.mensaje.value}&atencionA=${documentForm.atencionA.value}`;
        requestData += `&tipoDoc=${documentForm.tipoDoc.value}`;
        requestData += `&idCentroCostoOrigen=${idCentroCosto}`;
        requestData += `&idPersona=${documentForm.idPersona}&inicialesCC=${documentForm.inicialesCC}`;
        requestData += `&idExpediente=${idExpediente}&idCCDestino=${documentForm.idCCDestino}`;

        documentForm.asunto.value ==='' || documentForm.asunto.value.length > 250 ? requestData='' : null
        documentForm.mensaje.value ==='' ? requestData='' : null
        documentForm.tipoDoc.value==='' ? requestData='' : null
        documentForm.idPersona ==='' ? requestData='' : null
        documentForm.inicialesCC ==='' ? requestData='' : null
        documentForm.idExpediente ==='' ? requestData='' : null
        documentForm.idCCDestino==='' ? requestData='' : null
    return requestData;
}
function getCopia(){
    let arrayListado=document.getElementById('listaCopiaCC').innerHTML;
    let arrayDeCadenas='';
    if(arrayListado != ''){
        arrayDeCadenas = arrayListado.slice(0, -1).split('-');
    }
    return arrayDeCadenas;
}

/***************************************************************************
****************************************************************************/
function requestDataServer(ruta, requestData, accion){
    const request = new XMLHttpRequest();
    request.onload = () => {
        let responseObject = null;
        try {
        responseObject = JSON.parse(request.responseText);
        } catch (e) {
            console.error('Could not parse JSON!');
        }
        if (responseObject) {
            if(accion=='select'){
                localStorage.setItem('idCCDestino', responseObject[0].idCentroCosto);
                let name=responseObject[0].nombreCompletoNPM.toLowerCase().replace(/\b\w/g, l => l.toUpperCase());
                document.querySelector('#nameDir').defaultValue = `${responseObject[0].tituloAcademicoAbreviado} ${name}`;
                document.querySelector('#puestodes').defaultValue = `${responseObject[0].puesto}`;
                document.querySelector('#puestodesI').defaultValue = `${responseObject[0].puestoIngles}`;
            }
            if(accion=='no'){
                titulo=responseObject[0].tituloAcademicoAbreviado;
                puesto=responseObject[0].puesto;
                puestoIngles=responseObject[0].puestoIngles;
            }
            if(accion=='selectSeccion'){

                let selectSeccion='<select id="selectSeccion"> <option disabled selected value> -- Selecciona una sección -- </option>';
                
                responseObject.map(item =>{
                    selectSeccion+= `<option value="${item.idSeccion}">${item.nombre}</option>`;
                });

                selectSeccion+='</select>';
                document.getElementById("selectSeccion").innerHTML =selectSeccion;


                let selectSecc = document.getElementById('selectSeccion');
                selectSecc.addEventListener('change', function(){
                let selectedOption = this.options[selectSecc.selectedIndex];
                    console.log(selectedOption.value + ': ' + selectedOption.text);
                    requestDataServer('php/get_SerieArchivistica.php', `idSeccion=${selectedOption.value}`, 'console');
                });
            }
            if(accion=='console'){
                console.log(responseObject)
                let selectSeccion='<select id="serieArchivistica"> <option disabled selected value> -- Selecciona una sección -- </option>';
                
                responseObject.map(item =>{
                    selectSeccion+= `<option value="${item.idSerieArchivistica}">${item.serieArchivistica}</option>`;
                });

                selectSeccion+='</select>';
                document.getElementById("serieArchivistica").innerHTML =selectSeccion;
            }
        }
    };
    request.open('post', ruta, false);
    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    request.send(requestData);
}
/***************************************************************************
    addEventListener para obtener los Centros de Costos, colocandolos en el
    select.
****************************************************************************/
function formarDoc(){
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(aaync) {
        if (this.readyState == 4 && this.status == 200) {
            let responseObject = JSON.parse(this.responseText);
            let selectCentroCostos='<select id="selectCC"> <option disabled selected value> -- Selecciona una opción -- </option>';
            responseObject.map(item =>{
                selectCentroCostos+= `<option value="${item.idCentroCosto}">${item.centroCosto}</option>`;
            });
            selectCentroCostos+='</select><img src="src/images/svg/lupa_select.svg"/>';
            document.getElementById("select_div").innerHTML =selectCentroCostos;
            /*********************************************************************
                obtener el valor del select
             *********************************************************************/
            let select = document.getElementById('selectCC');
            select.addEventListener('change', function(){
              let selectedOption = this.options[select.selectedIndex];
              requestInsert('php/get_destino.php',`folio=${selectedOption.value}`,pushSelect, null, null);
              //requestDataServer('php/get_destino.php', `folio=${selectedOption.value}`, 'select');
            });
        }
    };
    xhttp.open("GET", 'php/data_document.php', false);
    xhttp.send();
}
/***************************************************************************
    Formar el select de centro de costos de la vista crear documento
****************************************************************************/
/*function centroCostos() {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(aaync) {
        if (this.readyState == 4 && this.status == 200) {
            let responseObject = JSON.parse(this.responseText);
            let selectCentroCostos='<select id="selectCCcopia"> <option disabled selected value> -- Selecciona una opción -- </option>';
            responseObject.map(item =>{
                selectCentroCostos+= `<option value="${item.idCentroCosto}">${item.centroCosto}</option>`;
            });
            selectCentroCostos+='</select><img src="src/images/svg/lupa_select.svg"/>';
            document.getElementById("selectCCcopia").innerHTML =selectCentroCostos;
        }
    };
    xhttp.open("GET", 'php/data_document.php', false);
    xhttp.send();
}*/
function selectCrearDocumento(requestData) {
    let responseObject = JSON.parse(requestData);
    let selectCentroCostos='<select id="selectCC"> <option disabled selected value> -- Selecciona una opción -- </option>';
    responseObject.map(item =>{
        selectCentroCostos+= `<option value="${item.idCentroCosto}">${item.centroCosto}</option>`;
    });
    selectCentroCostos+='</select><img src="src/images/svg/lupa_select.svg"/>';
    document.getElementById("select_div").innerHTML =selectCentroCostos;
    /*********************************************************************
        obtener el valor del select
    *********************************************************************/
    let select = document.getElementById('selectCC');
        select.addEventListener('change', function(){
            let selectedOption = this.options[select.selectedIndex];
            requestDataServer('php/get_destino.php', `folio=${selectedOption.value}`, 'select');
        });
}
/***************************************************************************
    Validar los radio y mostrar o no el input ubicación
****************************************************************************/
function validar_radio(event) {
    var input = document.getElementById('ubicacion');
    if(event.target.value == 'Fisico' || event.target.value == 'Hibrido'){
        input.disabled = false;
        tipo_expediente=event.target.value;
        //console.log('Checked radio with ID = ' + event.target.value);
    }
    if(event.target.value == 'Electronico'){
        input.disabled = true;
        tipo_expediente=event.target.value;
        //console.log('Checked radio with ID = ' + event.target.value);
    }
}

/***************************************************************************
    Mostrar el modal para un nuevo editor
****************************************************************************/
function AgregarEditor(){
    const elementFloat = document.querySelector('#float'); 
    document.getElementById("float").style.display="block";
    elementFloat.classList.add('animate__animated', 'animate__slideInRight');
    elementFloat.addEventListener('animationend', () => {
        document.getElementById("float").style.display="block";
    });
    show__table_Editor("#tableEditor","persona");
}
/***************************************************************************
    Mostrar el modal para un nuevo expediente
****************************************************************************/
function nuevo_Expediente(){
    //location.href = "#modalCrearExpediente"; 
    location.href = "#Expediente"; 
    textCleanExpediente();
    document.querySelectorAll("input[name='selector']").forEach((input) => {
        input.addEventListener('change', validar_radio);
    });
    requestDataServer('php/get_seccion.php', null, 'selectSeccion');
    
    document.getElementById("btn_expedienteCrear").style.display="block";
    
    document.getElementById("btn_expedienteEditar").style.display="none";
}
/***************************************************************************
    Limpiar los form
****************************************************************************/
const textClean = () =>{
    document.getElementById("asunto").value= "";
    document.getElementById("puestodes").value= "";
    document.getElementById("puestodesI").value= "";
    document.getElementById("mensaje").value= "";
    document.getElementById("atencionA").value= "";
    document.getElementById("CC").value= "";
}
const textCleanExpediente = () =>{
    document.getElementById("titulo").value= "";
    document.getElementById("resumen").value= "";
    document.getElementById("observaciones").value= "";
    document.getElementById("ubicacion").value= "";
    
}
/****************************************************************************/
/*window.onload = function(){
    if(!Push.Permission.GRANTED){
        Push.Permission.request();
    }
}
setInterval(function(){
    Push.create("Gestión Documental", {
        body: "Recibiste un nuevo oficio",
        icon: 'src/images/svg/notificacion.png',
        timeout: 9000,
        vibrate: [100,100,100],
        onClick : function(){
            alert('Click en la notificiacion');
        }
    });
},15000);*/