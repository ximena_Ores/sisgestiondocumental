let valores_obt = [];
/***************************************************************************
    función de prueba
****************************************************************************/
const hola = (e) =>{
    alert ("hola");
    console.log(e.id);
    //let info = typeof window !== 'undefined' ? JSON.parse(localStorage.getItem('infoSesion')) : null;
    //console.log("holass")
}
const visualizar_pdf_oficio = (e) =>{
    //window.location.replace("modulo_documento/index.php");
    console.log(e.id, "el id....");
    let doc_info = valores_obt.find(item => item.folio === e.id);
    console.log(doc_info);

    const request = new XMLHttpRequest();
    request.onload = () => {
        let responseObject = null;
        try {
        responseObject = JSON.parse(request.responseText);
        } catch (e) {
        console.error('Could not parse JSON!');
        }
        if (responseObject) {
            console.log(responseObject);
            setTimeout(function(){ location.href = "#miModal"; }, 500);
            //setTimeout(function(){ window.location.replace("modulo_documento/documento.php"); }, 3000);
            
            

        }
    };
    const requestData = `fecha=${doc_info.fechaElaboracion}&folio=${doc_info.folio}&asunto=${doc_info.asunto}`;
    //const requestData = `fecha=${doc_info.fechaElaboracion}&folio=${doc_info.folio}&asunto=${doc_info.asunto}&destino=${doc_info.nombrePersonaDestino}`;
        //requestData += `&costoDestino=${doc_info.centroCostoDestino}&contenido=${doc_info.contenido}&origen=${doc_info.nombrePersonaOrigen}&costoOrigen=${doc_info.centroCostoOrigen}`;
    
    request.open('post', 'modulo_documento/get__data.php');
    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    request.send(requestData);


}
/***************************************************************************
    función para mostrar modal
****************************************************************************/

const show__modal__persona = () =>{
    $("#table__personas").DataTable({
        "destroy":true,
        "ajax":{
          "method":"POST",
          "url":"tabla.php",
          "data" : {type: "btn_persona"}
        },
      "columns":[
          {"data":"numeroEmpleado"},
          {"data":"nombre"},
          {"defaultContent": "<button id='agregar' class='btn__add btn__table'></button>"}
      ], "language": idioma_espanol
    });
    
    location.href = "#miModal";

}
/***************************************************************************************************/
//si está iniciada una sesión ver qué mostrará
/***************************************************************************************************/
let info = typeof window !== 'undefined' ? JSON.parse(localStorage.getItem('infoSesion')) : null;
    if (!info) {
        info = [];
    }
    if (info) {
        switch (info.rol) {
            case 'Director':
                getVistas("menu", "vistas/Director.php");
                director();
                console.log("hola soy director");
            break;
            case 'Editor':
                getVistas("menu", "vistas/Editor.php");
                console.log("hola soy editor");
            break;
            case 'Supervisor':
                getVistas("menu", "vistas/Supervisor.php");
                console.log("hola soy supervisor");
            break;
            default:
                alert("disculpa existio un error");
                window.location.replace("login.php");
        }
        //document.getElementById("name").innerHTML= info.name;
    } 
    if(info.length == 0) {
        window.location.replace("login.php");
    }
/*****************************************************************************
	función con xmlhttPRecuest que obtiene vistas con parametro del id del
	contenedor y ruta/nombre de la vista
/*****************************************************************************/
function getVistas(contenedor, vista){
	const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(aaync) {
    	if (this.readyState == 4 && this.status == 200) {
            document.getElementById(contenedor).innerHTML = this.responseText;
		}
    };
	xhttp.open("GET", vista, false);
	xhttp.send();
}
const outSesion = document.querySelector('#btn__out');
    outSesion.addEventListener('click', function (){
        let info = typeof window !== 'undefined' ? JSON.parse(localStorage.getItem('infoSesion')) : null;
        if (info) {
            localStorage.setItem('infoSesion', JSON.stringify([]));
        }
        window.location.replace("login.html");
});
/*****************************************************************************
	querySelector obtiene el contenido de la etiqueta con el ID o CLASS,
    agregamos el evento con el addEventList la función que manda a llamar.
    crea el btn y manda la nueva vista a un contenedor.
/*****************************************************************************/
function btnCrear(nombre, funcion, content, view){
    const btn = document.querySelector(`#${nombre}`);
    btn.addEventListener('click', function (){
      funcion(content,view);
    });
}
/*****************************************************************************
    funciones para llevar a llamar los botones al hacer clic para los 
    diferentes tipos de usuarios
/*****************************************************************************/
function director(){
    btnCrear("btn_buscar", getVistas, "cont_body", "vistas/vistas_view_documentos/buscar.html");
    btnCrear("btn_recibidos", getVistas, "cont_body", "vistas/vistas_view_documentos/recibidos.html");
    btnCrear("btn_aprobar", getVistas, "cont_body", "vistas/vistas_view_documentos/aprobar.html");
    btnCrear("btn_enviados", getVistas, "cont_body", "vistas/vistas_view_documentos/enviados.html");
    btnCrear("btn_expedientes", getVistas, "cont_body", "vistas/vistas_view_documentos/expedientes.html");
    btnCrear("btn_borradores", getVistas, "cont_body", "vistas/vistas_view_documentos/borradores.html");
    btnCrear("btn_configuracion", getVistas, "cont_body", "vistas/vistas_view_documentos/configuracion.html");
}
function editor(){
    btnCrear("btn_buscar", getVistas, "cont_body", "vistas/vistas_view_documentos/buscar.html");
    btnCrear("btn_expedientes", getVistas, "cont_body", "vistas/vistas_view_documentos/expedientes.html");
    btnCrear("btn_borradores", getVistas, "cont_body", "vistas/vistas_view_documentos/borradores.html");
}
function supervisor(){
    btnCrear("btn_buscar", getVistas, "cont_body", "vistas/vistas_view_documentos/buscar.html");
    btnCrear("btn_recibidos", getVistas, "cont_body", "vistas/vistas_view_documentos/recibidos.html");
    btnCrear("btn_aprobar", getVistas, "cont_body", "vistas/vistas_view_documentos/aprobar.html");
    btnCrear("btn_enviados", getVistas, "cont_body", "vistas/vistas_view_documentos/enviados.html");
    btnCrear("btn_expedientes", getVistas, "cont_body", "vistas/vistas_view_documentos/expedientes.html");
    btnCrear("btn_borradores", getVistas, "cont_body", "vistas/vistas_view_documentos/borradores.html");
}
/*****************************************************************************
    funciones para buscar documento por folio
/*****************************************************************************/
const get_data_document_buscar = () =>{
    const form_doc = {
        folio: document.querySelector(`#folio`).value
    };
    console.log(form_doc.folio);
    console.log(document.querySelector(`#fecha_inicio`).value);
    console.log(document.querySelector(`#fecha_fin`).value);

    const request = new XMLHttpRequest();
    request.onload = () => {
        let responseObject = null;
        try {
        responseObject = JSON.parse(request.responseText);
        } catch (e) {
        console.error('Could not parse JSON!');
        alert('Could not parse JSON!');
        }
        if (responseObject) {
            let mensaje=responseObject.res;
            let table_content = `
            <tr>
                <th>FOLIO</th>
                <th>TIPO DOCUMENTO</th>
                <th>ASUNTO</th>
                <th>ORIGEN</th>
                <th>DESTINO</th>
                <th>FECHA</th>
                <th></th>
            </tr>`;
            mensaje.map(item =>{
                table_content+=`
                <tr>
                    <td><b>${item.folio}</b></td>
                    <td>${item.tipoDocumento}</td>
                    <td>${item.asunto}</td>
                    <td>${item.centroCostoOrigen}</td>
                    <td>${item.centroCostoDestino}</td>
                    <td>${item.fechaElaboracion}</td>
                    <td><button id="${item.folio}" onClick="visualizar_pdf_oficio(this)" class="btn__ver_doc">xs<!--a href="#miModal">Abrir Modal</a--></button></td>
                </tr>`;
            });
            console.log(responseObject.res);
            document.getElementById("table").innerHTML=table_content;
            valores_obt=responseObject.res;
            
            //handleResponse(responseObject);
        }
    };
    const requestData = `folio=${form_doc.folio}`;
    
    request.open('post', 'buscar.php');
    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    request.send(requestData);
}

/***************************************************************************
    función de btn para mandar a llamar la tabla correspondiente
****************************************************************************/
$("#btn_expedientes").on("click", function(){
    show__table("btn_expedientes");
});
$("#btn_recibidos").on("click", function(){
    show__table("btn_recibidos");
});
$("#btn_aprobar").on("click", function(){
    show__table("btn_aprobar");
});
$("#btn_enviados").on("click", function(){
    show__table("btn_enviados");
});
$("#btn_borradores").on("click", function(){
    show__table("btn_borradores");
});
$("#btn_configuracion").on("click", function(){
    show__table__administacion("btn_configuracion");
});
/***************************************************************************
    función para mostrar la tabla respectivamente de su opción que va al php
    y regresa la información
****************************************************************************/
const show__table = (e) =>{
    $("#tableR").DataTable({
          "destroy":true,
          "ajax":{
            "method":"POST",
            "url":"tabla.php",
            "data" : {type: e}
          },
        "columns":[
            {"data":"folio"},
            {"data":"tipo"},
            {"data":"asunto"},
            {"data":"origen"},
            {"data":"destino"},
            {"data":"fecha"},
            {"defaultContent": "<button id='idBtnEditar' class='btnEditar editar'></button><button id='idBtnBorrar' class='btnEditar borrar'></button>"}
        ], "language": idioma_espanol
        });
}

const show__table__administacion = (e) =>{
    $("#tableR").DataTable({
          "destroy":true,
          "ajax":{
            "method":"POST",
            "url":"tabla.php",
            "data" : {type: e}
          },
        "columns":[
            {"data":"numeroEmpleado"},
            {"data":"nombre"},
            {"data":"centroCosto"},
            {"defaultContent": "<button id='idBtnEditar' class='btn__table btn__add'></button><button id='idBtnBorrar' class='btn__table btn__cancelar'></button>"}
        ], "language": idioma_espanol
        });
}
/***************************************************************************
    variable para mostrar en español la tabla
****************************************************************************/
const idioma_espanol = {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}