<button class="btn__menu" id="btn_buscar"><img src="src/lupa.svg" width="20px"/>Buscar documento</button>
<button class="btn__menu" id="btn_recibidos"><img src="src/recibidos.svg" width="20px"/>Documentos recibidos</button>
<button class="btn__menu" id="btn_aprobar"><img src="src/aprobar.svg" width="20px"/>Documentos por aprobar</button>
<button class="btn__menu" id="btn_enviados"><img src="src/enviar.svg" width="20px"/>Enviados</button>
<button class="btn__menu" id="btn_expedientes"><img src="src/expedientes.svg" width="20px"/>Expedientes</button>
<button class="btn__menu" id="btn_borradores"><img src="src/borradores.svg" width="20px"/>Borradores</button>
<button class="btn__menu" id="btn_configuracion"><img src="src/configuraciones.svg" width="20px"/>Administración</button>