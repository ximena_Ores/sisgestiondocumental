<?php
require('fpdf.php');
session_start();


function encrypt($password){
    $hash = password_hash($password, PASSWORD_DEFAULT, ['cost' => 5]);
	return $hash;
}
/*function encriptar($cadena){
    $key='2021';  // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
    $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $cadena, MCRYPT_MODE_CBC, md5(md5($key))));
    return $encrypted;

};*/
class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    $this->Image('../src/encabezados/head'.$_SESSION['idCC'].'.png',2,4,210);
}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,utf8_decode('Imagen footer ').$this->PageNo().'/{nb}',0,0,'R');
}
}

$pdf = new PDF('P', 'mm', 'Letter');
$pdf->SetTopMargin(60);
//$pdf->SetMargins(0, 60 , 0);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->setAutoPageBreak(true);
$pdf->SetFont('Times','',12);
$pdf->SetXY(10,48);
$pdf->Cell(195,5,$_SESSION['fecha_doc'],0,1,'R');
$pdf->Cell(195,5,$_SESSION['folio_doc'],0,1,'R');
$pdf->Cell(195,5,utf8_decode('Asunto: '.$_SESSION['asunto_doc']),0,1,'R');

$pdf->Ln(8);

$pdf->Cell(130,5,utf8_decode($_SESSION['destinoName_doc']),0,1,'L');
$pdf->Cell(130,5,utf8_decode($_SESSION['destinoPuesto_doc']),0,1,'L');
$pdf->Cell(130,5,utf8_decode($_SESSION['destinoPuestoIngles_doc']),0,1,'L');

$pdf->Ln(15);

if($_SESSION['atencionA'] != ''){
    $pdf->MultiCell(195,6,utf8_decode($_SESSION['atencionA']),0,'J');
}
$pdf->MultiCell(195,6,utf8_decode($_SESSION['mensaje_doc']),0,'J');
if($_SESSION['inicialesCC'] !=null){
    foreach ($_SESSION['inicialesCC'] as $item) {
        //print_r($item . '<br>');
        $pdf->MultiCell(195,6,utf8_decode($item),0,'J');
    }
}
    
    /*$folio=encrypt($_SESSION['folio_doc'],'acb123');
    $estado=encrypt($_SESSION['estado'],'acb123');
    $firma=encrypt($_SESSION['firma'],'acb123');*/
    $folio=$_SESSION['folio_doc'];
    $estado=$_SESSION['estado'];
    $idCC=$_SESSION['idCC'];
    $idCCDestino=$_SESSION['firma'];

    $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
    include "qrlib.php";
    if (!file_exists($PNG_TEMP_DIR))
        mkdir($PNG_TEMP_DIR);
    $filename = 'firma.png';
    $errorCorrectionLevel = 'M';
    $matrixPointSize = 25;
    $estado_enc=encrypt($folio);
    $idCC_enc=encrypt($folio);
    $idCCDestino_enc=encrypt($folio);
    if($_SESSION['estado'] == 'EN'){
        $contenido="validacion.php?&folio=$folio&e=$estado_enc&f=$idCC_enc";
    } else if($_SESSION['estado'] == 'LE'){
        $contenido="validacion.php?&folio=$folio&e=$estado_enc&f=$idCCDestino_enc";
    }
    if($_SESSION['estado'] == 'EN' || $_SESSION['estado'] == 'LE'){
        QRcode::png( $contenido, $filename, $errorCorrectionLevel, $matrixPointSize); 
        $pdf->Image($filename,78,150,60);
    }
    
    
    
    
    //$pdf->SetXY(10,180);
    //$pdf->Image('../src/encabezados/head'.$_SESSION['idCC'].'.png',50,195,180);


//$pdf->SetXY(10,180);
//$pdf->Cell(195,5,utf8_decode($PNG_TEMP_DIR),0,1,'C');

$pdf->SetXY(10,220);
$pdf->Cell(195,5,utf8_decode('"Amor, Orden Y Progreso"'),0,1,'C');
$pdf->Cell(195,5,utf8_decode($_SESSION['origenName_doc']),0,1,'C');
$pdf->Cell(195,5,utf8_decode($_SESSION['origenPuesto_doc']),0,1,'C');
$pdf->Cell(195,5,utf8_decode($_SESSION['origenPuestoIngles_doc']),0,1,'C');

$pdf->Output($_SESSION['folio_doc'], "I");
?>